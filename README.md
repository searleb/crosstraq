![Crosstraq Splash Image](https://bitbucket.org/searleb/crosstraq/downloads/crosstraq_graphic.png)

# Crosstraq #
### Crosstalk and ratio compression analysis for iTRAQ and TMT ###
Crosstraq is an open-source Java library for removing sources of interference from iTRAQ and TMT data. Data interpretation in TMT and iTRAQ experiments is complicated by two factors: chemical contamination and isotopic contamination. Chemical contamination in proteomics measurements occurs when multiple peptides with similar precursor masses are co-isolated and co-fragmented. Isotopic contamination is caused because isobaric tagging reagents cannot be produced with 100% purity. Crosstraq is a tool to help mitigate both sources of error.

### How do I get Crosstraq? ###
Crosstraq is open source under the [Apache 2 licence](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)), which means you can do what you like with the software, as long as you include the required notices. Here is a link to download the latest [stable version](https://bitbucket.org/searleb/crosstraq/downloads/crosstraq-0.0.2.jar) and here is a link to the [manual](https://bitbucket.org/searleb/crosstraq/downloads/crosstraq_manual.pdf).

Crosstraq is a cross-platform Java application that has been tested for Windows, Macintosh, and Linux. Crosstraq requires 64-bit Java 1.8. If you don’t already have it, you can download "[Windows x64 Offline](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)”. After you have 64-bit Java 1.8, double click on the Crosstraq .JAR file to launch the GUI interface. If you are using a Macintosh, you may need to right click on the Crosstraq .JAR and select “Open” to execute it for the first time with the proper permissions.

### Who do I talk to? ###
This is a [Searle Lab](https://systemsbiology.org/bio/brian-searle/) project from the [Institute for Systems Biology](http://www.systemsbiology.org/). For more information please contact Brian Searle (bsearle at systemsbiology dot org).

### Contribution guidelines ###
Contributions are welcome! Any contribution must follow the coding style of the project, be presented with tests and stand up to code review before it will be accepted.