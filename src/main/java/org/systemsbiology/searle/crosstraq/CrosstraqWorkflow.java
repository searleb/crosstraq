package org.systemsbiology.searle.crosstraq;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.Nullable;

import org.systemsbiology.searle.crosstraq.io.MGFWriter;
import org.systemsbiology.searle.crosstraq.io.MSMSBlock;
import org.systemsbiology.searle.crosstraq.io.MSMSConsumer;
import org.systemsbiology.searle.crosstraq.io.MzidReader;
import org.systemsbiology.searle.crosstraq.structs.FragmentScan;
import org.systemsbiology.searle.crosstraq.structs.MassErrorUnitType;
import org.systemsbiology.searle.crosstraq.structs.MassTolerance;
import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;
import org.systemsbiology.searle.crosstraq.structs.RetentionTimeComparator;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.CrosstalkMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.ITRAQ4PlexMatrix;
import org.systemsbiology.searle.crosstraq.utils.EmptyProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.Log;
import org.systemsbiology.searle.crosstraq.utils.Logger;
import org.systemsbiology.searle.crosstraq.utils.MassConstants;
import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.Range;
import org.systemsbiology.searle.crosstraq.utils.SubProgressIndicator;

public class CrosstraqWorkflow {

	private static final double PRECURSOR_ISOLATION_WINDOW=1.25; // for +/-1.25
	private static final MassTolerance PRECURSOR_TOLERANCE=new MassTolerance(20);
	private static final MassTolerance FRAGMENT_TOLERANCE=new MassTolerance(0.2, MassErrorUnitType.AMU);

	public static void main(String[] args) throws Exception {
		//File mzidFile=new File("/Users/bsearle/Documents/itraq/gluc_only/K20110712_iTRAQ_GluCmix1_1-2-5.scaffold.mzid/My Experiment.mzid");
		//File msFile=new File("/Users/bsearle/Documents/itraq/gluc_only/K20110712_iTRAQ_GluCmix1_1-2-5.mzML");

		
		//File mzidFile=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-3-10/My Experiment mzIdent export 19-Nov-2018 11-34-04-AM/My Experiment.mzid");
		//File msFile=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-3-10.mzML");
		
		//File mzidFile=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-3-10/My Experiment mzIdent export 19-Nov-2018 04-53-34-PM/My Experiment.mzid");
		//File msFile=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-3-10.mzML");
		
		byte referenceChannel=-1;
		File context=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/");
		context=new File("/Users/searleb/Documents/itraq/gluc_in_trypsin_background/");
		
		Long time=System.currentTimeMillis();
		//File mzidFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-3-10/My Experiment mzIdent export 19-Nov-2018 11-34-04-AM/My Experiment.mzid");
		//File msFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-3-10.mzML");
		File mzidFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-2-5/My Experiment mzIdent export 19-Nov-2018 11-37-45-AM/My Experiment.mzid");
		File msFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-2-5.mzML");
		float[] expectedRatios = new float[] {1f/3f,1f/3f,1f/3f,0};
		processMzID(new EmptyProgressIndicator(false), mzidFile, msFile, referenceChannel, expectedRatios, new ITRAQ4PlexMatrix(), true); //new float[] {1f/3f,1f/3f,1f/3f,0}
		
		//mzidFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-3-10/My Experiment mzIdent export 19-Nov-2018 04-53-34-PM/My Experiment.mzid");
		//msFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn8_GluC_1-3-10.mzML");
		mzidFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-2-5/My Experiment mzIdent export 19-Nov-2018 10-57-32-AM/My Experiment.mzid");
		msFile=new File(context, "K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-2-5.mzML");
		processMzID(new EmptyProgressIndicator(false), mzidFile, msFile, referenceChannel, expectedRatios, new ITRAQ4PlexMatrix(), true);
		
		
		System.out.println("Total analysis time: "+((System.currentTimeMillis()-time)/1000f)+" seconds");
	}

	public static ArrayList<float[]> processMzID(ProgressIndicator progress, File mzidFile, File msFile, byte referenceChannel, @Nullable float[] expectedRatios, CrosstalkMatrix matrix, boolean usePurityCorrection) {
		double[] ionMZs = matrix.getIonMZs();
		boolean[] channelsReported=new boolean[ionMZs.length];
		Arrays.fill(channelsReported, true);
		if (referenceChannel>=0) channelsReported[referenceChannel]=false;
		ArrayList<float[]> returnArray=new ArrayList<float[]>();
		
		SubProgressIndicator sub1=new SubProgressIndicator(progress, 0.4f);
		HashMap<String, HashMap<String, PeptideSpectrumMatch>> psmMapByFile=MzidReader.readMzID(sub1, mzidFile);
		sub1.finish("Read "+psmMapByFile.size()+" psm files");
		
		System.out.println("Found "+psmMapByFile.size()+" psm files:");
		ArrayList<String> keySet = new ArrayList<String>(psmMapByFile.keySet());
		Collections.sort(keySet);
		for (String s : keySet) {
			System.out.println("  "+s+" --> "+psmMapByFile.get(s).size()+" entries");
		}
		
		ArrayList<PeptideSpectrumMatch> psms=new ArrayList<>();
		for (HashMap<String, PeptideSpectrumMatch> map : psmMapByFile.values()) {
			psms.addAll(map.values());
		}
		Collections.sort(psms, RetentionTimeComparator.comparator);

		// middle 90% of peptides
		Range primaryRTRange=new Range(psms.get(Math.round(psms.size()*0.05f)).getRetentionTimeInSec(), psms.get(Math.round(psms.size()*0.95f)).getRetentionTimeInSec());
		
		// data is synchronized in write block (not thread safe for reading)
		ArrayList<float[]> globalReporterIons=new ArrayList<>();
		ConcurrentHashMap<PeptideSpectrumMatch, Pair<Float, PrecursorScan>> precursorRegionsByPSM=new ConcurrentHashMap<>((int)(psms.size()/0.80f));
		
		BlockingQueue<MSMSBlock> blockQueue=new ArrayBlockingQueue<MSMSBlock>(1);
		MSMSConsumer consumer=new MSMSConsumer(blockQueue) {
			PrecursorScan lastScan=null;
			
			@Override
			public void processMSMSBlock(MSMSBlock block) {
				ArrayList<PrecursorScan> ms1s=block.getPrecursorScans();
				ArrayList<FragmentScan> ms2s=block.getFragmentScans();
				ArrayList<float[]> reporterIonList=new ArrayList<>();
				for (FragmentScan msms : ms2s) {
					if (primaryRTRange.contains(msms.getRetentionTimeInSec())) {
						float[] reporterIons=FRAGMENT_TOLERANCE.getIntegratedIntensities(msms.getMassArray(), msms.getIntensityArray(), ionMZs).getSecond();
						reporterIonList.add(reporterIons);
					}
				}
				synchronized (globalReporterIons) {
					globalReporterIons.addAll(reporterIonList);
				}
				
				if (ms1s.size()>0) {
					if (lastScan!=null) ms1s.add(0, lastScan);
					Collections.sort(ms1s, RetentionTimeComparator.comparator);
					lastScan=ms1s.get(ms1s.size()-1);
					List<PeptideSpectrumMatch> local=getNearbyPSMs(psms, ms1s.get(0).getRetentionTimeInSec(), lastScan.getRetentionTimeInSec());
					
					for (PeptideSpectrumMatch psm : local) {
						int insertionPoint=Collections.binarySearch(ms1s, RetentionTimeComparator.getComparable(psm.getRetentionTimeInSec()), RetentionTimeComparator.comparator);
						if (insertionPoint<0) {
							insertionPoint=-(insertionPoint+1);
						}
						
						PrecursorScan nearestPrecursor=null;
						if (insertionPoint>=0&&insertionPoint<=ms1s.size()) {
							nearestPrecursor=ms1s.get(insertionPoint-1);
						}
						if (insertionPoint<ms1s.size()) {
							
							if (nearestPrecursor==null) {
								nearestPrecursor=ms1s.get(insertionPoint);
							} else {
								float deltaRT=Math.abs(psm.getRetentionTimeInSec()-nearestPrecursor.getRetentionTimeInSec());
								PrecursorScan altNearestPrecursor=ms1s.get(insertionPoint);
								float altDeltaRT=Math.abs(psm.getRetentionTimeInSec()-altNearestPrecursor.getRetentionTimeInSec());
								if (altDeltaRT<deltaRT) {
									nearestPrecursor=altNearestPrecursor;
								}
							}
						}
						
						if (nearestPrecursor!=null) {
							float delta=Math.abs(psm.getRetentionTimeInSec()-nearestPrecursor.getRetentionTimeInSec());
							Pair<Float, PrecursorScan> entry=precursorRegionsByPSM.get(psm);
							if (entry==null||entry.getFirst()>delta) {
								precursorRegionsByPSM.put(psm, new Pair<>(delta, nearestPrecursor.getSubscan(psm.getComputedMz()-PRECURSOR_ISOLATION_WINDOW, psm.getComputedMz()+PRECURSOR_ISOLATION_WINDOW)));
							}
						}
					}
				}
			}
		};

		SubProgressIndicator sub2=new SubProgressIndicator(progress, 0.5f);
		MzidReader.readMS(sub2, msFile, blockQueue, consumer);
		sub2.finish("Read "+precursorRegionsByPSM.size()+" MS1 scans");

		float[] foundRatios=new float[ionMZs.length];
		for (float[] reporterIons : globalReporterIons) {
			for (int i = 0; i < foundRatios.length; i++) {
				foundRatios[i]+=reporterIons[i];
			}
		}
		foundRatios=General.normalize(foundRatios);
		if (expectedRatios==null) {
			expectedRatios=foundRatios;
		}

		try {

			BlockingQueue<MSMSBlock> mgfQueue=new LinkedBlockingQueue<MSMSBlock>();
			File mgfFile=new File(mzidFile.getAbsolutePath()+"_corrected.mgf");
			MGFWriter mgfconsumer=new MGFWriter(mgfQueue, null, mgfFile);
			Thread consumerThread=new Thread(mgfconsumer);
			consumerThread.start();
			
			PrintWriter writer=new PrintWriter(new File(mzidFile.getAbsolutePath()+".txt"));
			writer.print("title\tpeptide\taccessions\tlog10Intensity\tpurityRatio");
			for (int i = 0; i < channelsReported.length; i++) {
				if (channelsReported[i]) {
					writer.print("\t"+matrix.getIonNames()[i]);
				}
			}
			writer.println();
	
			SubProgressIndicator sub3=new SubProgressIndicator(progress, 0.1f);
			int count=0;
			int lastUpdate=0;

			ArrayList<FragmentScan> newScans=new ArrayList<>();
			for (Entry<PeptideSpectrumMatch, Pair<Float, PrecursorScan>> entry : precursorRegionsByPSM.entrySet()) {
				PeptideSpectrumMatch psm=entry.getKey();
	
				int floor=(int)((count*100L)/(float)precursorRegionsByPSM.size());
				if (floor>lastUpdate) {
					String message="Statistical processing "+floor+"%";
					lastUpdate=floor;
					progress.update(message, count/(float)precursorRegionsByPSM.size());
				}
				count++;
				
				if (!psm.getMSMS().isPresent()) continue;
				
				PrecursorScan precursor=entry.getValue().getSecond();
				
				double expectedMz=psm.getComputedMz();
				float[] expectedIsotopicDistribution=psm.getPeptide().getIsotopeDistribution();
				double[] expectedMzs=new double[expectedIsotopicDistribution.length];
				
				for (int i=0; i<expectedIsotopicDistribution.length; i++) {
					expectedMzs[i]=expectedMz+(MassConstants.neutronMass*i)/psm.getCharge();
				}
				
				float[] intensities=PRECURSOR_TOLERANCE.getIntegratedIntensities(precursor.getMassArray(), precursor.getIntensityArray(), expectedMzs).getSecond();
				
				float minScaler=Float.MAX_VALUE;
				for (int i=0; i<expectedIsotopicDistribution.length; i++) {
					if (expectedIsotopicDistribution[i]>0.5f&&intensities[i]>0.0f) {
						float scaler=expectedIsotopicDistribution[i]/intensities[i];
						if (minScaler>scaler) {
							minScaler=scaler;
						}
					}
				}
				
				float totalWindowIntensity=General.sum(precursor.getIntensityArray());
				float totalExpectedIntensity=0.0f;
				for (int i=0; i<expectedMzs.length; i++) {
					// can't ever have more intensity than recorded
					totalExpectedIntensity+=Math.min(intensities[i], expectedIsotopicDistribution[i]/minScaler);
				}
				ArrayList<String> accessions=new ArrayList<String>(psm.getPeptide().getProteinAccessions());
				Collections.sort(accessions);
				
				FragmentScan msms=psm.getMSMS().get();
				float[] rawReporterIons=FRAGMENT_TOLERANCE.getIntegratedIntensities(msms.getMassArray(), msms.getIntensityArray(), ionMZs).getSecond();
				float[] reporterIons=matrix.correct(rawReporterIons);
				
				// if we're expecting a reference (default behavior does not expect this) then quit if the reference is missing
				if (referenceChannel>=0&&reporterIons[referenceChannel]==0.0f) continue;
				
				rawReporterIons=General.normalize(rawReporterIons);
				reporterIons=General.normalize(reporterIons);
				
				
				// don't allow the purity to be worse than 25% to stop overcorrecting
				float purityRatio=Math.max(0.25f, totalExpectedIntensity/totalWindowIntensity); // S2I

				float minUsedIntensity=Float.MAX_VALUE;
				for (int i=0; i<reporterIons.length; i++) {
					if (expectedRatios[i]>0.5f/ionMZs.length) {
						if (reporterIons[i]<minUsedIntensity) {
							minUsedIntensity=reporterIons[i];
						}
					}
				}
				
				// SEARLE METHOD
				float searleInterference=minUsedIntensity*(1.0f-purityRatio);
				float[] searleCorrectedReporterIons = General.subtract(reporterIons, searleInterference);
				for (int i = 0; i < searleCorrectedReporterIons.length; i++) {
					if (searleCorrectedReporterIons[i]<0.0f) searleCorrectedReporterIons[i]=0.0f;
				}
				searleCorrectedReporterIons=General.add(searleCorrectedReporterIons, 0.001f);
				searleCorrectedReporterIons=General.normalize(searleCorrectedReporterIons);
				// END SEARLE METHOD
				
				// SAVITSKI
				float sumIntensity=General.sum(reporterIons);
				float estimatedTotalInterference=sumIntensity*(1.0f-purityRatio);
				float[] estimatedInterference=General.multiply(foundRatios, estimatedTotalInterference);
				float[] savitskiCorrectedReporterIons = General.subtract(reporterIons, estimatedInterference);
				for (int i = 0; i < savitskiCorrectedReporterIons.length; i++) {
					if (savitskiCorrectedReporterIons[i]<0.0f) savitskiCorrectedReporterIons[i]=0.0f;
				}
				savitskiCorrectedReporterIons=General.add(savitskiCorrectedReporterIons, 0.001f);
				savitskiCorrectedReporterIons=General.normalize(savitskiCorrectedReporterIons);
				// END SAVITSKI METHOD
				
				//float[] naiveRatios=referenceChannel<0?rawReporterIons:Log.protectedLog2(General.divide(rawReporterIons, rawReporterIons[referenceChannel]));
				//float[] crosstalkRemovedRatios=referenceChannel<0?reporterIons:Log.protectedLog2(General.divide(reporterIons, reporterIons[referenceChannel]));
				float[] fullyCorrectedRatios;
				if (!usePurityCorrection) {
					fullyCorrectedRatios=reporterIons;
				} else {
					if (referenceChannel<0) {
						fullyCorrectedRatios=searleCorrectedReporterIons;
					} else {
						fullyCorrectedRatios=Log.protectedLog2(General.divide(searleCorrectedReporterIons, searleCorrectedReporterIons[referenceChannel]));
					}
				}
				
				returnArray.add(fullyCorrectedRatios);
				
				writer.println(psm.getSpectrumTitle()+"\t"+psm.getPeptide().getPeptideSequence()+"\t"+General.toString(accessions)+"\t"+Math.log10(totalExpectedIntensity)+"\t"+purityRatio+"\t"+
						toString(fullyCorrectedRatios, channelsReported));

				System.out.println(psm.getSpectrumTitle()+"\t"+psm.getPeptide().getPeptideSequence()+"\t"+General.toString(accessions)+"\t"+Math.log10(totalExpectedIntensity)+"\t"+purityRatio
						+"\t"+toString(rawReporterIons, channelsReported)+"\t"+toString(reporterIons, channelsReported)+"\t"+toString(savitskiCorrectedReporterIons, channelsReported)+"\t"+toString(searleCorrectedReporterIons, channelsReported));
				//		+"\t"+General.cosineSimilarity(naiveRatios, crosstalkRemovedRatios)+"\t"+General.cosineSimilarity(crosstalkRemovedRatios, fullyCorrectedRatios)+"\t"+General.cosineSimilarity(naiveRatios, fullyCorrectedRatios));
				
				if (psm.getMSMS().isPresent()) {
					double min=General.min(ionMZs)-0.5f;
					double max=General.max(ionMZs)+0.5f;
					newScans.add(psm.getMSMS().get().replaceMSMSRegion(new Range(min, max), ionMZs, savitskiCorrectedReporterIons));
					
					if (newScans.size()>1000) {
						mgfQueue.add(new MSMSBlock(new ArrayList<>(), newScans));
						newScans.clear();
					}
				}
			}
			if (newScans.size()>0) {
				mgfQueue.add(new MSMSBlock(new ArrayList<>(), newScans));
			}
			mgfQueue.add(MSMSBlock.POISON_BLOCK);

			consumerThread.join();
			sub3.finish("Processed "+psms.size()+" quantification data points");
			for (PeptideSpectrumMatch psm : precursorRegionsByPSM.keySet()) {
				psms.remove(psm);
			}
			for (PeptideSpectrumMatch psm : psms) {
				writer.println("MISSING: "+psm.getSpectrumTitle()+", mz:"+psm.getComputedMz()+", rt:"+psm.getRetentionTimeInSec()+", "+psm.getPeptide().getPeptideSequence());
			}
			writer.flush();
			writer.close();
		} catch (IOException ioe) {
			Logger.errorException(ioe);
		} catch (InterruptedException ie) {
			Logger.errorException(ie);
		}

		return returnArray;
	}

	public static String toString(float[] values, boolean[] channelsUsed) {
		if (values==null) return null;
		
		StringBuilder sb=new StringBuilder();
		for (int i=0; i<values.length; i++) {
			if (channelsUsed[i]) {
				if (sb.length()>0) {
					sb.append('\t');
				}
				sb.append(values[i]);
			}
		}
		return sb.toString();
	}
	
	public static List<PeptideSpectrumMatch> getNearbyPSMs(ArrayList<PeptideSpectrumMatch> psms, float minRT, float maxRT) {
		int lowerIndex=Collections.binarySearch(psms, RetentionTimeComparator.getComparable(minRT), RetentionTimeComparator.comparator);
		if (lowerIndex<0) {
			lowerIndex=-lowerIndex-1;
		}
		if (lowerIndex<0) lowerIndex=0;
		
		int upperIndex=Collections.binarySearch(psms, RetentionTimeComparator.getComparable(maxRT), RetentionTimeComparator.comparator);
		if (upperIndex<0) {
			upperIndex=-upperIndex-1;
		}
		if (upperIndex>psms.size()) upperIndex=psms.size(); 
		
		if (upperIndex>=lowerIndex) {
			return psms.subList(lowerIndex, upperIndex);
		} else {
			return new ArrayList<>();
		}
	}
}
