package org.systemsbiology.searle.crosstraq.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.TextAttribute;
import java.awt.geom.Ellipse2D;
import java.text.AttributedString;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.AbstractXYItemRenderer;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.XYTraceInterface;

public class Charter {

	public static void launchComponent(JComponent comp, String title, Dimension dim) {
		final JFrame f=new JFrame(title);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		f.getContentPane().add(comp, BorderLayout.CENTER);

		f.pack();
		f.setSize(dim);
		f.setVisible(true);
	}

	public static void launchChart(ChartPanel chart, String title) {
		launchComponent(chart, title, new Dimension(792, 612));
	}

	public static void launchChart(ChartPanel chart, String title, Dimension dim) {
		launchComponent(chart, title, dim);
	}

	public static void launchCharts(String title, Map<String, ChartPanel> panelMap) {
		launchComponent(getTabbedChartPane(panelMap), title, new Dimension(792, 612));
	}

	public static JTabbedPane getTabbedChartPane(Map<String, ChartPanel> panelMap) {
		JTabbedPane tabs=new JTabbedPane();
		for (Entry<String, ChartPanel> entry : panelMap.entrySet()) {
			tabs.addTab(entry.getKey(), entry.getValue());
		}
		return tabs;
	}
	public static ChartPanel getChart(final String xAxis, String yAxis, boolean displayLegend, double maxY, final XYTraceInterface... traces) {
		Font font=new Font("News Gothic MT", Font.PLAIN, 24);
		Font font2=new Font("News Gothic MT", Font.PLAIN, 32);
		Font font3=new Font("News Gothic MT", Font.PLAIN, 18);
		Font font4=new Font("News Gothic MT", Font.PLAIN, 14);
		font=new Font("News Gothic MT", Font.PLAIN, 16);
		font2=new Font("News Gothic MT", Font.PLAIN, 16);
		font3=new Font("News Gothic MT", Font.PLAIN, 16);
		font4=new Font("News Gothic MT", Font.PLAIN, 10);
		HashMap<TextAttribute, Object> m=new HashMap<TextAttribute, Object>(font2.getAttributes());
		m.put(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER);
		Font font2super=new Font(m);

		double divider;
		AttributedString yAxisLabel;
		if (maxY>1e15) {
			divider=1e15;
			yAxisLabel=new AttributedString(yAxis+" (1015)");
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2super, yAxis.length()+4, yAxis.length()+6);
		} else if (maxY>1e12) {
			divider=1e12;
			yAxisLabel=new AttributedString(yAxis+" (1012)");
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2super, yAxis.length()+4, yAxis.length()+6);
		} else if (maxY>1e9) {
			divider=1e9;
			yAxisLabel=new AttributedString(yAxis+" (109)");
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2super, yAxis.length()+4, yAxis.length()+5);
		} else if (maxY>1e6) {
			divider=1e6;
			yAxisLabel=new AttributedString(yAxis+" (106)");
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2super, yAxis.length()+4, yAxis.length()+5);
		} else if (maxY>1e3) {
			divider=1e3;
			yAxisLabel=new AttributedString(yAxis+" (103)");
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2super, yAxis.length()+4, yAxis.length()+5);
		} else {
			divider=1;
			yAxisLabel=new AttributedString(yAxis);
			yAxisLabel.addAttribute(TextAttribute.FONT, font2);
		}

		XYPlot plot=new XYPlot();
		NumberAxis numberaxis=new NumberAxis(xAxis);
		numberaxis.setAutoRangeIncludesZero(false);
		NumberAxis numberaxis1;
		if (divider==1) {
			numberaxis1=new NumberAxis(yAxis);
		} else {
			numberaxis1=new NumberAxis();
			numberaxis1.setAttributedLabel(yAxisLabel);
		}
		numberaxis1.setAutoRangeIncludesZero(false);
		plot.setDomainAxis(numberaxis);
		plot.setRangeAxis(numberaxis1);

		int count=0;
		for (XYTraceInterface trace : traces) {
			AbstractXYItemRenderer renderer=new XYLineAndShapeRenderer();
			switch (trace.getType()) {
			case area:
				renderer=new XYAreaRenderer();
				renderer.setSeriesStroke(0, new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				break;

			case line:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesStroke(0, new BasicStroke(trace.getThickness().orElse(2.0f), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				((XYLineAndShapeRenderer) renderer).setBaseShapesVisible(false);

				break;

			case boldline:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesStroke(0, new BasicStroke(trace.getThickness().orElse(5.0f), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				((XYLineAndShapeRenderer) renderer).setBaseShapesVisible(false);

				break;

			case squaredline:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesStroke(0, new BasicStroke(trace.getThickness().orElse(5.0f), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
				((XYLineAndShapeRenderer) renderer).setBaseShapesVisible(false);

				break;

			case dashedline:
				renderer=new XYLineAndShapeRenderer();
				Float thickness=trace.getThickness().orElse(2.0f);
				if (thickness>5) {
					renderer.setSeriesStroke(0, new BasicStroke(thickness, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f, new float[] {12.0f, 16.0f}, 0.0f));
				} else {
					renderer.setSeriesStroke(0, new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 0.0f, new float[] {3.0f, 5.0f}, 0.0f));
				}
				((XYLineAndShapeRenderer) renderer).setDrawSeriesLineAsPath(true);
				((XYLineAndShapeRenderer) renderer).setBaseShapesVisible(false);

				break;

			case bigpoint:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesShape(0, new Ellipse2D.Double(0, 0, 6, 6));
				((XYLineAndShapeRenderer) renderer).setBaseLinesVisible(false);

				break;

			case point:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesShape(0, new Ellipse2D.Double(0, 0, 3, 3));
				((XYLineAndShapeRenderer) renderer).setBaseLinesVisible(false);

				break;

			case tinypoint:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesShape(0, new Ellipse2D.Double(0, 0, 1, 1));
				((XYLineAndShapeRenderer) renderer).setBaseLinesVisible(false);

				break;

			case text:
				renderer=new XYLineAndShapeRenderer();
				renderer.setSeriesShape(0, new Ellipse2D.Double(0, 0, 1, 1));
				((XYLineAndShapeRenderer) renderer).setBaseLinesVisible(false);

				break;

			default:
				throw new RuntimeException("unsupported graphing type!");
			}

			Pair<double[], double[]> values=trace.toArrays();
			double[] x=values.getFirst();
			double[] y=General.divide(values.getSecond(), divider);
			XYSeriesCollection dataset=new XYSeriesCollection();
			switch (trace.getType()) {
			case area:
			case line:
			case boldline:
			case squaredline:
			case dashedline:
			case bigpoint:
			case point:
			case tinypoint:
				XYSeries series=new XYSeries(trace.getName());
				for (int i=0; i<x.length; i++) {
					if (!Double.isNaN(x[i])&&!Double.isNaN(y[i])) {
						series.add(x[i], y[i]);
					}
				}
				dataset.addSeries(series);
				break;

			case text:
				for (int i=0; i<x.length; i++) {
					if (!Double.isNaN(x[i])&&!Double.isNaN(y[i])) {
						XYTextAnnotation annotation=new XYTextAnnotation(trace.getName(), x[i], y[i]*1.01);
						annotation.setFont(font4);
						plot.addAnnotation(annotation);
					}
				}
				break;

			default:
				throw new RuntimeException("unsupported graphing type!");
			}
			if (trace.getColor().isPresent()) {
				renderer.setSeriesPaint(0, trace.getColor().get());
				renderer.setBasePaint(trace.getColor().get());
			}

			plot.setDataset(count, dataset);
			plot.setRenderer(count, renderer);

			count++;
		}

		plot.setBackgroundPaint(Color.white);
		plot.setDomainGridlinePaint(Color.white);//gray);
		plot.setDomainGridlinesVisible(false);
		plot.setRangeGridlinePaint(Color.white);//gray);
		plot.setRangeGridlinesVisible(false);
		JFreeChart chart=new JFreeChart(plot);
		chart.setBackgroundPaint(Color.white);

		NumberAxis rangeAxis=(NumberAxis) ((XYPlot) plot).getRangeAxis();
		rangeAxis.setLabelFont(font2);
		rangeAxis.setTickLabelFont(font);

		NumberAxis domainAxis=(NumberAxis) ((XYPlot) plot).getDomainAxis();
		if (domainAxis!=null) {
			domainAxis.setLabelFont(font2);
			domainAxis.setTickLabelFont(font);
		}

		final ChartPanel chartPanel=new ChartPanel(chart, false);
		if (!displayLegend) {
			chartPanel.getChart().removeLegend();
		} else {
			chartPanel.getChart().getLegend().setItemFont(font3);
		}
		addCopyDataMenu(xAxis, chartPanel, traces);
		
		//rangeAxis.setTickUnit(new NumberTickUnit(20)); 
		//domainAxis.setTickUnit(new NumberTickUnit(20));
		//rangeAxis.setRange(0, 135);
		//domainAxis.setRange(15, 135);

		chartPanel.setMinimumDrawWidth(0);
		chartPanel.setMinimumDrawHeight(0);
		chartPanel.setMaximumDrawWidth(Integer.MAX_VALUE);
		chartPanel.setMaximumDrawHeight(Integer.MAX_VALUE);

		if (maxY>0&&divider>0) numberaxis1.setUpperBound(maxY/divider);
		return chartPanel;
	}

	private static void addCopyDataMenu(final String xAxis, final ChartPanel chartPanel,
			final XYTraceInterface... traces) {
		JMenuItem copyItem=new JMenuItem("Copy data values");
		chartPanel.getPopupMenu().add(copyItem, 2);
		copyItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				StringBuilder header=new StringBuilder("Row");
				int length=0;
				for (XYTraceInterface trace : traces) {
					if (trace.size()>length) length=trace.size();
				}
				StringBuilder[] rows=new StringBuilder[length];
				for (int i=0; i<rows.length; i++) {
					rows[i]=new StringBuilder(Integer.toString(i+1));
				}
				
				for (XYTraceInterface trace : traces) {
					header.append("\t"+xAxis+"\t"+trace.getName());
					
					Pair<double[], double[]> pairs=trace.toArrays();
					for (int i=0; i<rows.length; i++) {
						if (pairs.getFirst().length>i) {
							rows[i].append("\t"+pairs.getFirst()[i]+"\t"+pairs.getSecond()[i]);
						} else {
							rows[i].append("\t\t");
						}
					}
				}
				StringBuilder sb=new StringBuilder(header.toString());
				sb.append("\n");
				for (int i=0; i<rows.length; i++) {
					sb.append(rows[i]);
					sb.append("\n");
				}
				StringSelection stringSelection = new StringSelection(sb.toString());
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(stringSelection, null);
			}
		});
	}

}
