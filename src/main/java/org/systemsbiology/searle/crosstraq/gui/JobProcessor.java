package org.systemsbiology.searle.crosstraq.gui;

import java.util.ArrayList;

public interface JobProcessor {

	ArrayList<SwingJob> getQueue();

	void addJob(SwingJob job);

	void fireJobUpdated(SwingJob job);

}