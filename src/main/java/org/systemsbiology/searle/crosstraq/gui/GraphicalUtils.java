package org.systemsbiology.searle.crosstraq.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.systemsbiology.searle.crosstraq.utils.Logger;

public class GraphicalUtils {
	public static void launchPanel(JPanel panel) {
		launchPanel(panel, "Test Panel", new Dimension(1900, 1030));
	}
	
	public static void launchPanel(JPanel panel, String name, Dimension dimension) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			Logger.errorLine("Error setting look and feel!");
			Logger.errorException(e);
		}
		
		if (System.getProperty("os.name").toLowerCase().indexOf("mac")>=0) {
			System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Windowing Scheme Wizard");
			System.setProperty("apple.laf.useScreenMenuBar", "true");
		}

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				final JFrame f=new JFrame(name);
				f.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});

				f.getContentPane().add(panel, BorderLayout.CENTER);

				f.pack();
				f.setSize(dimension);
				f.setVisible(true);
			}
		});

		Logger.logLine("Launching "+name);
	}
}
