package org.systemsbiology.searle.crosstraq.gui;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Optional;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.CrosstalkMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.ITRAQ4PlexMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.ITRAQ8PlexWithYMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.TMT10PlexMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.TMT11PlexMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.TMT6PlexMatrix;

import com.oracle.layout.SpringUtilities;

public class CrosstalkEditorPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final CrosstalkMatrix[] DEFAULT_OPTIONS=new CrosstalkMatrix[] {new ITRAQ4PlexMatrix(), new ITRAQ8PlexWithYMatrix(), new TMT6PlexMatrix(), new TMT10PlexMatrix(), new TMT11PlexMatrix()};
	DecimalFormat df=new DecimalFormat("0.####");
	private boolean wasCanceled=true;
	
	public static void main(String[] args) {
		final JFrame frame = null;

		CrosstalkMatrix seed=new TMT10PlexMatrix();
		Optional<Pair<CrosstalkMatrix, Boolean>> maybeMatrix=askUserForCrosstalk(frame, seed);
		if (maybeMatrix.isPresent()) {
			System.out.println(maybeMatrix.get().getFirst().toMatrixString());
		} else {
			System.out.println("User cancelled.");
		}
	}
	
	public static Optional<Pair<CrosstalkMatrix, Boolean>> askUserForCrosstalk(Window parent, CrosstalkMatrix seed) {
		final JDialog dialog=new JDialog(parent, "Specify Crosstalk Matrix", ModalityType.APPLICATION_MODAL);
		
		final CrosstalkEditorPanel organizer=new CrosstalkEditorPanel(seed);
		
		JPanel buttons=new JPanel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton okButton=new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				organizer.wasCanceled=false;
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		buttons.add(okButton);
		JButton cancelButton=new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				organizer.wasCanceled=true;
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		buttons.add(cancelButton);
		
		Vector<CrosstalkMatrix> items = new Vector<>();
		boolean found=false;
		for (int i = 0; i < DEFAULT_OPTIONS.length; i++) {
			if (DEFAULT_OPTIONS[i].getName().equals(seed.getName())) {
				items.add(seed);
				found=true;
			} else {
				items.add(DEFAULT_OPTIONS[i]);
			}
		}
		if (!found) {
			items.add(seed);
		}
		final JComboBox<CrosstalkMatrix> matrixOptions=new JComboBox<>(items);
		matrixOptions.setSelectedItem(seed);
		matrixOptions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				organizer.rebuild((CrosstalkMatrix)matrixOptions.getSelectedItem());
				dialog.pack();
			}
		});

		JPanel mainpane=new JPanel(new BorderLayout());
		mainpane.add(matrixOptions, BorderLayout.NORTH);
		mainpane.add(organizer, BorderLayout.CENTER);
		mainpane.add(buttons, BorderLayout.SOUTH);
		
		dialog.getContentPane().add(mainpane, BorderLayout.CENTER);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.pack(); 
		dialog.setVisible(true);
		
		if (!organizer.wasCanceled) {
			return organizer.getMatrix();
		} else {
			return Optional.empty();
		}
	}

	private CrosstalkMatrix seed; // mutable!
	private JTextField[][] fields; // mutable!
	private JCheckBox applyPurityCorrection;
	public CrosstalkEditorPanel(CrosstalkMatrix seed) {
		super(new BorderLayout());
		this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), BorderFactory.createTitledBorder("Crosstalk parameters:")));
		
		rebuild(seed);
	}

	private void rebuild(CrosstalkMatrix seed) {
		this.removeAll();
		this.seed=seed;
		
		JPanel grid=new JPanel(new SpringLayout());
		grid.add(new JLabel());
		
		String[] names=seed.getIonNames();
		float[][] matrix=seed.get2n2CorrectionFactors();
		fields=new JTextField[matrix.length][];
		for (int i = 0; i < matrix.length; i++) {
			fields[i]=new JTextField[matrix[i].length];
		}
		
		int middlePoint=matrix[0].length/2;
		for (int j = 0; j < matrix[0].length; j++) {
			String text;
			if (j>=middlePoint) {
				text="+"+(j-middlePoint+1);
			} else {
				text="-"+(middlePoint-j);
			}
		    JLabel label = new JLabel(text);
			label.setHorizontalAlignment(SwingConstants.CENTER);
			grid.add(label);
		}
		
		for (int i = 0; i < matrix.length; i++) {
			JLabel label = new JLabel(names[i]);
			label.setHorizontalAlignment(SwingConstants.RIGHT);
			grid.add(label);
			for (int j = 0; j < matrix[0].length; j++) {
				JTextField textField = new JTextField(df.format(matrix[i][j]));
				fields[i][j]=textField;
				textField.setHorizontalAlignment(SwingConstants.RIGHT);
				PlainDocument doc = (PlainDocument) textField.getDocument();
				doc.setDocumentFilter(new MyIntFilter());
				grid.add(textField);
			}
		}
		
		SpringUtilities.makeGrid(grid,
				matrix.length+1, matrix[0].length+1, //rows, cols
                5, 5, //initialX, initialY
                5, 5);//xPad, yPad
		
		this.add(grid, BorderLayout.NORTH);
		
		applyPurityCorrection=new JCheckBox("Also apply precursor impurity correction (not for MS3)", true);
		this.add(applyPurityCorrection, BorderLayout.SOUTH);
		this.repaint();
	}
	
	private Optional<Pair<CrosstalkMatrix, Boolean>> getMatrix() {
		float[][] matrix=new float[fields.length][];
		for (int i = 0; i < matrix.length; i++) {
			matrix[i]=new float[fields[i].length];
		}
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j]=Float.parseFloat(fields[i][j].getText());
			}
		}
		return Optional.of(new Pair<CrosstalkMatrix, Boolean>(seed.generateNewMatrix(matrix), applyPurityCorrection.isSelected()));
	}
	
	class MyIntFilter extends DocumentFilter {
		@Override
		public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.insert(offset, string);

			if (test(sb.toString())) {
				super.insertString(fb, offset, string, attr);
			} else {
				// warn the user and don't allow the insert
			}
		}

		private boolean test(String text) {
			try {
				Float.parseFloat(text);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}

		@Override
		public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.replace(offset, offset + length, text);

			if (test(sb.toString())) {
				super.replace(fb, offset, length, text, attrs);
			} else {
				// warn the user and don't allow the insert
			}
		}

		@Override
		public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.delete(offset, offset + length);

			if (test(sb.toString())) {
				super.remove(fb, offset, length);
			} else {
				// warn the user and don't allow the insert
			}
		}
	}
}
