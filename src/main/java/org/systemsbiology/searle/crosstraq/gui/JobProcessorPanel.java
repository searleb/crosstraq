package org.systemsbiology.searle.crosstraq.gui;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;


public class JobProcessorPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private final JobProcessorTableModel model;
	
	private boolean wasCanceled=true;

	public static boolean processJobs(Window parent, ArrayList<SwingJob> jobs) {
		final JDialog dialog=new JDialog(parent, "Job Processor", ModalityType.APPLICATION_MODAL);
		
		final JobProcessorPanel organizer=new JobProcessorPanel(jobs);
		
		JPanel buttons=new JPanel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton cancelButton=new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				organizer.model.cancel();
				organizer.wasCanceled=true;
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		buttons.add(cancelButton);

		JPanel mainpane=new JPanel(new BorderLayout());
		mainpane.add(organizer, BorderLayout.CENTER);
		mainpane.add(buttons, BorderLayout.SOUTH);
		
		dialog.getContentPane().add(mainpane, BorderLayout.CENTER);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.pack(); 
		dialog.setVisible(true);
		
		return !organizer.wasCanceled;
	}

	public JobProcessorPanel(ArrayList<SwingJob> jobs) {
		super(new BorderLayout());
		
		model=new JobProcessorTableModel();
		for (SwingJob swingJob : jobs) {
			model.addJob(swingJob);
		}
		
		JTable table=new JTable(model);
		TableColumn column=table.getColumnModel().getColumn(1);
		column.setCellRenderer(new ProgressRenderer());
		this.add(new JScrollPane(table), BorderLayout.CENTER);
	}
}
