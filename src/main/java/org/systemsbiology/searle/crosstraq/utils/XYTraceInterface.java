package org.systemsbiology.searle.crosstraq.utils;

import java.awt.Color;
import java.util.Optional;

import org.systemsbiology.searle.crosstraq.structs.Pair;

public interface XYTraceInterface {

	Optional<Color> getColor();

	Optional<Float> getThickness();

	String getName();

	GraphType getType();

	Pair<double[], double[]> toArrays();

	int size();
}