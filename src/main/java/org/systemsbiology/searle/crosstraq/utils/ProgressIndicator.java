package org.systemsbiology.searle.crosstraq.utils;

public interface ProgressIndicator {
	public void update(String message);
	public void update(String message, float totalProgress);
	public float getTotalProgress();
}
