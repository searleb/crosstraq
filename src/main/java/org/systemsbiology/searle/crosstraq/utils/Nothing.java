package org.systemsbiology.searle.crosstraq.utils;

/**
 * also see Optional
 * @author searleb
 *
 */
public class Nothing {
	public static final Nothing NOTHING=new Nothing();

	private Nothing() {
	}
	
}
