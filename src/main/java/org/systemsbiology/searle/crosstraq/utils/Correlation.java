package org.systemsbiology.searle.crosstraq.utils;

import java.util.Arrays;

public class Correlation {
	public static float getSpearmans(float[] x, float[] y) {
		return getPearsons(rank(x), rank(y));
	}
	
	public static float getPearsons(float[] x, float[] y) {
		float xBar=General.mean(x);
		float yBar=General.mean(y);
		
		float numerator=0.0f;
		float xSS=0.0f;
		float ySS=0.0f;
		for (int i=0; i<y.length; i++) {
			float xDiff=x[i]-xBar;
			float yDiff=y[i]-yBar;
			numerator+=xDiff*yDiff;
			xSS+=xDiff*xDiff;
			ySS+=yDiff*yDiff;
		}
		if (xSS==0||ySS==0) {
			return 0.0f;
		}
		return numerator/(float)Math.sqrt(xSS*ySS);
	}
	
	private static float[] rank(float[] values) {
		float[] sorted=values.clone();
		Arrays.sort(sorted);
		float[] sortedRanks=new float[sorted.length];

		// basic rank
		for (int i=0; i<sorted.length; i++) {
			sortedRanks[i]=i+1;
		}

		for (int i=0; i<sorted.length; i++) {
			int start=i;
			int stop=i;

			// find ties
			boolean ties=false;
			while (++stop<sorted.length&&sorted[start]==sorted[stop]) {
				ties=true;
			}

			// substitute rank average for ties
			if (stop-start>1&&ties) {
				float avg=0;
				for (int j=start; j<stop; j++) {
					avg+=sortedRanks[j];
				}
				avg=avg/(stop-start);

				for (int x=start; x<stop; x++) {
					sortedRanks[x]=avg;
				}
			}
			
			// advance i to end of stop
			i=stop-1;
		}
		float[] ranksInOrder=new float[sortedRanks.length];
		for (int i=0; i<sortedRanks.length; i++) {
			int index=Arrays.binarySearch(sorted, values[i]);
			ranksInOrder[i]=sortedRanks[index];
		}

		return ranksInOrder;
	}
}
