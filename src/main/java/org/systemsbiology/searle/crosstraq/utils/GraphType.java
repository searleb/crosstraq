package org.systemsbiology.searle.crosstraq.utils;

public enum GraphType {
	area, line, dashedline, boldline, squaredline, bigpoint, point, tinypoint, text;
}
