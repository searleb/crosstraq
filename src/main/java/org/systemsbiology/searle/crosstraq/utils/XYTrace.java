package org.systemsbiology.searle.crosstraq.utils;

import java.awt.Color;
import java.util.Optional;

import org.systemsbiology.searle.crosstraq.structs.Pair;

public class XYTrace implements XYTraceInterface {

	private final String name;
	private final double[] x;
	private final double[] y;
	private final GraphType type;
	private final Optional<Color> color;
	private final Optional<Float> thickness;

	public XYTrace(String name, double[] x, double[] y, GraphType type, Color color, float thickness) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.type = type;
		this.color = Optional.of(color);
		this.thickness = Optional.of(thickness);
	}
	public XYTrace(String name, double[] x, double[] y, GraphType type, Optional<Color> color, Optional<Float> thickness) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.type = type;
		this.color = color;
		this.thickness = thickness;
	}

	@Override
	public Optional<Color> getColor() {
		return color;
	}

	@Override
	public Optional<Float> getThickness() {
		return thickness;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public GraphType getType() {
		return type;
	}

	@Override
	public Pair<double[], double[]> toArrays() {
		return new Pair<double[], double[]>(x, y);
	}

	@Override
	public int size() {
		return x.length;
	}
}
