package org.systemsbiology.searle.crosstraq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.systemsbiology.searle.crosstraq.utils.Logger;

public class MainBootstrap {
	public static void main(String[] args) {
		new MainBootstrap().go(args);
	}

	/**
	 * Method to run the pride inspector
	 */
	private void go(String[] args) {
		// read bootstrap properties
		Properties bootstrapProps=getBootstrapSettings();
		String generalArgs="";
		if (args!=null&&args.length>0) {
			for (String arg : args)
				generalArgs+=arg+" ";
		}
		generalArgs=generalArgs.trim();

		// get max memory

		List<String> arguments=new ArrayList<>();

		// createAttributedSequence the command
		StringBuilder cmdBuffer=new StringBuilder();
		arguments.add("java");
		arguments.add("-cp");
		String classPath=System.getProperty("java.class.path");
		Logger.logLine(classPath);
		arguments.add(System.getProperty("java.class.path"));

		String maxMem=bootstrapProps.getProperty("main.max.memory");
		int mem=0;
		if (maxMem!=null) {
			mem=Integer.parseInt(maxMem);
		}
		if (mem>0) {
			arguments.add("-Xmx"+maxMem+"m");
			Logger.logLine("Setting memory to "+maxMem+" mb.");
		} else {
			Logger.logLine("Memory not set, falling back to default memory settings.");
		}
		
		arguments.add(Main.class.getName());

		Collections.addAll(arguments, args);

		// call the command
		Process process;
		try {
			Logger.logLine(arguments.toString());
			Logger.logLine(cmdBuffer.toString());
			process=Runtime.getRuntime().exec(arguments.toArray(new String[arguments.size()]));

			StreamProxy errorStreamProxy=new StreamProxy(process.getErrorStream(), System.err);
			StreamProxy outStreamProxy=new StreamProxy(process.getInputStream(), System.out);

			errorStreamProxy.start();
			outStreamProxy.start();

		} catch (IOException e) {
			Logger.errorLine("Error while bootstrapping "+Main.class.getName());
			Logger.errorException(e);
		}
	}

	private Properties getBootstrapSettings() {
		Properties props=new Properties();

		InputStream inputStream=null;
		try {
	        CodeSource src = MainBootstrap.class.getProtectionDomain().getCodeSource();
			URL pathURL=new URL(src.getLocation(), "config/config.props");
			File file;
	        try {
	        	file=new File(pathURL.toURI());
	        } catch (URISyntaxException e) {
	        	file=new File(pathURL.getPath());
	        }
			
	        if (file.exists()) {
	        	// input stream of the property file
	        	inputStream=new FileInputStream(file);
				props.load(inputStream);
	        } else {
				Logger.errorLine("Couldn't find configuration file "+pathURL.toString()+", using default configuration...");
	        }
	        
		} catch (IOException e) {
			Logger.errorLine("Failed to load config/config.props file");
			Logger.errorException(e);
		} finally {
			if (inputStream!=null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					Logger.errorLine("Failed to close InputStream while reading config.props file");
					Logger.errorException(e);
				}
			}
		}

		return props;
	}

	private static class StreamProxy extends Thread {
		final InputStream is;
		final PrintStream os;

		StreamProxy(InputStream is, PrintStream os) {
			this.is=is;
			this.os=os;
		}

		public void run() {
			try {
				InputStreamReader isr=new InputStreamReader(is);
				BufferedReader br=new BufferedReader(isr);
				String line;
				while ((line=br.readLine())!=null) {
					os.println(line);
				}
			} catch (IOException ex) {
				throw new RuntimeException(ex.getMessage(), ex);
			}
		}
	}
}
