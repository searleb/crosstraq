package org.systemsbiology.searle.crosstraq;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.gui.Charter;
import org.systemsbiology.searle.crosstraq.io.MSMSBlock;
import org.systemsbiology.searle.crosstraq.io.MSMSConsumer;
import org.systemsbiology.searle.crosstraq.io.MzidReader;
import org.systemsbiology.searle.crosstraq.structs.MassTolerance;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;
import org.systemsbiology.searle.crosstraq.structs.RetentionTimeComparator;
import org.systemsbiology.searle.crosstraq.structs.integration.IntegratedPeptide;
import org.systemsbiology.searle.crosstraq.structs.integration.IntegrationParameters;
import org.systemsbiology.searle.crosstraq.structs.integration.IntegrationScores;
import org.systemsbiology.searle.crosstraq.structs.integration.Ion;
import org.systemsbiology.searle.crosstraq.structs.integration.PeakTrace;
import org.systemsbiology.searle.crosstraq.structs.integration.PrecursorIonMap;
import org.systemsbiology.searle.crosstraq.utils.EmptyProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.GraphType;
import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.SubProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.XYTraceInterface;

public class PrecursorQuantWorkflow {

	public static void main(String[] args) throws Exception {
		File context=new File("/Users/searleb/Documents/itraq/precursor_data/");
		File mzidFile=new File(context, "Distiller_results mzIdent export 17-Apr-2020 11-30-01-PM/25000amol/UPS1_25000amol_R3.mzid");
		File msFile=new File(context, "mzML_with_MS1/UPS1_25000amol_R3.mzML");

		IntegrationParameters params=new IntegrationParameters();
		processMzID(new EmptyProgressIndicator(false), mzidFile, msFile, params);
	}
	
	public static void processMzID(ProgressIndicator progress, File mzidFile, File msFile, IntegrationParameters params) {
		SubProgressIndicator sub1=new SubProgressIndicator(progress, 0.4f);
		HashMap<String, HashMap<String, PeptideSpectrumMatch>> psmMapByFile=MzidReader.readMzID(sub1, mzidFile);
		sub1.finish("Read "+psmMapByFile.size()+" psm files");
		
		HashMap<String, ArrayList<PeptideSpectrumMatch>> psmsBySequence=new HashMap<>();
		for (HashMap<String, PeptideSpectrumMatch> map : psmMapByFile.values()) {
			for (PeptideSpectrumMatch psm : map.values()) {
				ArrayList<PeptideSpectrumMatch> list=psmsBySequence.get(psm.getPeptide().getPeptideModSeq());
				if (list==null) {
					list=new ArrayList<>();
					psmsBySequence.put(psm.getPeptide().getPeptideModSeq(), list);
				}
				list.add(psm);
			}
		}
		
		final PrecursorIonMap map=new PrecursorIonMap(params.getTolerance(), params.getIntegrationTimeWindowInSec());
		for (Entry<String, ArrayList<PeptideSpectrumMatch>> entry : psmsBySequence.entrySet()) {
			map.addPeptide(entry.getKey(), entry.getValue());
		}
		map.finalizeMap();

		BlockingQueue<MSMSBlock> blockQueue=new ArrayBlockingQueue<MSMSBlock>(1);
		MSMSConsumer consumer=new MSMSConsumer(blockQueue) {
			@Override
			public void processMSMSBlock(MSMSBlock block) {
				ArrayList<PrecursorScan> ms1s=block.getPrecursorScans();
				for (PrecursorScan scan : ms1s) {
					map.processSpectrum(scan);
					System.out.println(scan.getRetentionTimeInSec()/60f);
				}
			}
		};

		SubProgressIndicator sub2=new SubProgressIndicator(progress, 0.5f);
		MzidReader.readMS(sub2, msFile, blockQueue, consumer);

		ArrayList<PeakTrace<Ion>> traces=map.getCenteredTraces();
		Collections.sort(traces, RetentionTimeComparator.comparator);
		
		ArrayList<IntegratedPeptide> peptides=map.getPeptides();
		Collections.sort(peptides, RetentionTimeComparator.comparator);
		
		for (IntegratedPeptide peptide : peptides) {
			if ("DPNVISTINVMSLAAVGK".equals(peptide.getPsms().get(0).getPeptide().getPeptideModSeq())) {
				System.out.println(peptide);
			}
			ArrayList<IntegrationScores> scores=peptide.integrate(params);
			for (IntegrationScores score : scores) {
				System.out.println("Integrate:"+(score.getApexRetentionTime()/60)+") "+score.getIon()+" --> "+score.getIntensity());
			}
		}
		

		@SuppressWarnings("rawtypes")
		ArrayList<PeakTrace> tracesPlusMedian=new ArrayList<>();
		@SuppressWarnings("rawtypes")
		ArrayList<PeakTrace> currentTraceBlock=new ArrayList<>();
		ArrayList<PeakTrace<String>> medianTracesByRT=new ArrayList<>();
		
		int previousIndex=0;
		for (PeakTrace<Ion> peakTrace : traces) {
			int rtIndex=(int)Math.floor(peakTrace.getRetentionTimeInSec()/60/10);
			if (previousIndex<rtIndex) {
				previousIndex=rtIndex;
				if (currentTraceBlock.size()>0) {
					PeakTrace<String> medianTrace = PeakTrace.getMedianTrace(currentTraceBlock);
					medianTracesByRT.add(new PeakTrace<String>(medianTrace, GraphType.boldline, new Color(rtIndex/15f, 0f, rtIndex/15f), 3.0f));
					currentTraceBlock.clear();
				}
			}
			currentTraceBlock.add(peakTrace);
			if (peakTrace.getRetentionTimeInSec()<60*40) {
				tracesPlusMedian.add(peakTrace);
			}
		}
		
		tracesPlusMedian.add(0, PeakTrace.getMedianTrace(tracesPlusMedian));

		XYTraceInterface[] traceArray=tracesPlusMedian.toArray(new XYTraceInterface[tracesPlusMedian.size()]);
		System.out.println("Starting to plot ("+traceArray.length+")...");
		Charter.launchChart(Charter.getChart("Delta RT", "Normalized Intensity (%)", false, 1.0, traceArray), "Peaks");

		traceArray=medianTracesByRT.toArray(new XYTraceInterface[medianTracesByRT.size()]);
		Charter.launchChart(Charter.getChart("Delta RT", "Normalized Intensity (%)", true, 1.0, traceArray), "Medians");
	}
}
