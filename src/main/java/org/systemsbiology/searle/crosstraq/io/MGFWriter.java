package org.systemsbiology.searle.crosstraq.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.structs.FragmentScan;
import org.systemsbiology.searle.crosstraq.structs.MassTolerance;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;
import org.systemsbiology.searle.crosstraq.structs.Spectrum;
import org.systemsbiology.searle.crosstraq.utils.Logger;

public class MGFWriter extends MSMSConsumer {
	private final File ms1MGF;
	private final File ms2MGF;
	private final MassTolerance tolerance=new MassTolerance(1);

	FragmentScan previousScan=null; // be careful, can be null, obvs!
	private PrintWriter ms1Writer=null; // be careful, can be null, obvs!
	private PrintWriter ms2Writer=null; // be careful, can be null, obvs!

	public MGFWriter(BlockingQueue<MSMSBlock> mzmlBlockQueue, File ms1MGF, File ms2MGF) {
		super(mzmlBlockQueue);
		this.ms1MGF=ms1MGF;
		this.ms2MGF=ms2MGF;
	}
	
	@Override
	public void processMSMSBlock(MSMSBlock block) {
		for (PrecursorScan ms : block.getPrecursorScans()) {
			if (ms1Writer!=null) writeMS(ms, ms1Writer);
		}
		for (FragmentScan msms : block.getFragmentScans()) {
			if (ms2Writer!=null) writeMSMS(msms, ms2Writer);
		}
	}
	
	@Override
	public void init() {
		super.init();
		try {
			ms1Writer=ms1MGF==null?null:new PrintWriter(ms1MGF, "UTF-8");
			ms2Writer=ms2MGF==null?null:new PrintWriter(ms2MGF, "UTF-8");
		} catch (FileNotFoundException e) {
			Logger.logException(e);
		} catch (UnsupportedEncodingException e) {
			Logger.logException(e);
		}
	}
	
	@Override
	public void close() {
		// write dangling scan
		if (previousScan!=null) {
			if (ms2Writer!=null) writeMSMSWithoutCheck(previousScan, ms2Writer);
		}
		
		super.close();
		if (ms1Writer!=null) {
			ms1Writer.close();
		}
		if (ms2Writer!=null) {
			ms2Writer.close();
		}
	}
	
	private void writeMS(PrecursorScan scan, PrintWriter ms1Writer) {
		ms1Writer.println("BEGIN IONS");
		writePeaks(scan, ms1Writer);
		ms1Writer.println("END IONS");
	}
	
	/**
	 * includes check for merge
	 * @param scan
	 * @param ms2Writer
	 */
	private void writeMSMS(FragmentScan scan, PrintWriter ms2Writer) {
		if (previousScan==null) {
			previousScan=scan;
			return;
		}
		if (previousScan.getPrecursorMz()==scan.getPrecursorMz()) {
			// merge the two and null out the previous
			scan=scan.mergeWith(previousScan, tolerance);
			previousScan=null;
		} else {
			// different, so process the previous and save the current as the new previous
			FragmentScan doSiDo=previousScan;
			previousScan=scan;
			scan=doSiDo;
		}
		
		writeMSMSWithoutCheck(scan, ms2Writer);
	}

	private void writeMSMSWithoutCheck(FragmentScan scan, PrintWriter ms2Writer) {
		byte charge=scan.getCharge();
		if (charge==0) return;

		ms2Writer.println("BEGIN IONS");
		if (charge>0) {
			// NOTE, PEPMASS!=PEPTIDE MASS, IT'S THE M/Z!!! THANKS, MATRIX SCIENCE
			ms2Writer.print("PEPMASS=");
			ms2Writer.println(scan.getPrecursorMz()); 

			ms2Writer.print("CHARGE=");
			ms2Writer.print(charge);
			if (charge>0) {
				ms2Writer.println("+");
			} else {
				ms2Writer.println("-");
			}
		}
		writePeaks(scan, ms2Writer);
		ms2Writer.println("END IONS");
	}

	private static void writePeaks(Spectrum scan, PrintWriter writer) {
		writer.print("TITLE=");
		writer.println(scan.getSpectrumName());
		writer.print("SCANS=");
		writer.println(scan.getSpectrumIndex());
		writer.print("RTINSECONDS=");
		writer.println(scan.getRetentionTimeInSec());

		double[] masses=scan.getMassArray();
		float[] intensities=scan.getIntensityArray();
		for (int i=0; i<masses.length; i++) {
			if (intensities[i]>0) {
				writer.print(masses[i]);
				writer.print('\t');
				writer.println(intensities[i]);
			}
		}
	}
}
