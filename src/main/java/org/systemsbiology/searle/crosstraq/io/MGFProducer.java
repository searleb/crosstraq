package org.systemsbiology.searle.crosstraq.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.systemsbiology.searle.crosstraq.structs.FragmentScan;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;
import org.systemsbiology.searle.crosstraq.utils.Logger;
import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;

public class MGFProducer implements MSMSProducer {
	private final File mgfFile;
	private final BlockingQueue<MSMSBlock> blockQueue;
	private final ProgressIndicator progress;

	private Throwable error;

	public MGFProducer(ProgressIndicator progress, File mgfFile, BlockingQueue<MSMSBlock> blockQueue) {
		this.mgfFile=mgfFile;
		this.blockQueue=blockQueue;
		this.progress=progress;
	}
	
	public Throwable getError() {
		return error;
	}

	@Override
	public void run() {
		try {
			final ProgressInputStream stream=new ProgressInputStream(new FileInputStream(mgfFile));
			final long length=mgfFile.length();

			stream.addChangeListener(new ChangeListener() {
				int lastUpdate=0;

				@Override
				public void stateChanged(ChangeEvent e) {
					int floor=(int)((stream.getProgress()*100L)/length);
					if (floor>lastUpdate) {
						String message="Parsing MGF "+floor+"%";
						lastUpdate=floor;
						progress.update(message, stream.getProgress()/(float)length);
					}
				}
			});

			BufferedReader reader=new BufferedReader(new InputStreamReader(stream));
			
			ArrayList<PrecursorScan> msList=new ArrayList<>();
			ArrayList<FragmentScan> msmsList=new ArrayList<>();
			String pepmass=null;
			String charge=null;
			String title=null;
			String scans=null;
			String rtinseconds=null;
			
			TDoubleArrayList masses=new TDoubleArrayList();
			TFloatArrayList intensities=new TFloatArrayList();
			
			String eachline;
			while ((eachline=reader.readLine())!=null) {
				eachline=eachline.trim();
				if (eachline.length()==0) continue;
				
				int equalsIndex=eachline.indexOf('=');
				if (equalsIndex>0) {
					String key=eachline.substring(0, equalsIndex);
					String value=eachline.substring(equalsIndex+1);
					
					if ("PEPMASS".equals(key)) {
						pepmass=value;
						int space=Math.max(pepmass.indexOf('\t'), pepmass.indexOf(' '));
						if (space>0) {
							pepmass=pepmass.substring(0, space);
						}
					} else if ("CHARGE".equals(key)) {
						charge=value;
					} else if ("TITLE".equals(key)) {
						title=value;
					} else if ("SCANS".equals(key)) {
						scans=value;
					} else if ("RTINSECONDS".equals(key)) {
						rtinseconds=value;
					}

				} else if ("BEGIN IONS".equals(eachline)) {
					pepmass=null;
					charge=null;
					title=null;
					scans=null;
					rtinseconds=null;
					masses.clear();
					intensities.clear();
					
				} else if ("END IONS".equals(eachline)) {
					// add spectrum
					
					if (pepmass==null) {
						//MS only
						int spectrumIndex=Integer.parseInt(scans);
						float scanStartTime=Float.parseFloat(rtinseconds);
						
						PrecursorScan ms=new PrecursorScan(title, spectrumIndex, scanStartTime, masses.toArray(), intensities.toArray());
						msList.add(ms);
					} else {
						int spectrumIndex=0;
						if (scans!=null) {
							try {
								spectrumIndex=Integer.parseInt(scans);
							} catch (NumberFormatException nfe) {
								Logger.errorLine("Can't parse MGF scan index ["+scans+"], setting scan number to 0");
							}
						}
						float scanStartTime=Float.parseFloat(rtinseconds);
						double mz=Double.parseDouble(pepmass); // NOTE, PEPMASS!=PEPTIDE MASS, IT'S THE M/Z!!! THANKS, MATRIX SCIENCE
						byte z;
						int andIndex=charge.indexOf("and");
						if (andIndex>0) { // chop off second possible charge state
							charge=charge.substring(0, andIndex-1).trim();
						}
						if (charge.charAt(charge.length()-1)=='+') {
							z=Byte.parseByte(charge.substring(0, charge.length()-1));
						} else if (charge.charAt(charge.length()-1)=='-') {
							z=(byte)-Byte.parseByte(charge.substring(0, charge.length()-1));
						} else {
							z=Byte.parseByte(charge);
						}
						
						FragmentScan msms=new FragmentScan(title, title, spectrumIndex, scanStartTime, mz, masses.toArray(), intensities.toArray(), z);
						msmsList.add(msms);
					}
					
					if (msList.size()>100||msmsList.size()>1000) {
						putBlock(new MSMSBlock(msList, msmsList));
						msList.clear();
						msmsList.clear();
					}

				} else {
					int tabIndex=eachline.indexOf('\t');
					if (tabIndex<0) {
						tabIndex=eachline.indexOf(' ');
					}
					try {
						double mass=Double.parseDouble(eachline.substring(0, tabIndex));
						float intensity=Float.parseFloat(eachline.substring(tabIndex+1));
						masses.add(mass);
						intensities.add(intensity);
					} catch (NumberFormatException nfe) {
						// ignore accidental parsing
					}
				}
			}
			if (msList.size()>0||msmsList.size()>0) {
				putBlock(new MSMSBlock(msList, msmsList));
				msList.clear();
				msmsList.clear();
			}
			
			reader.close();

			putBlock(MSMSBlock.POISON_BLOCK);
		} catch (Throwable t) {
			Logger.errorLine("Mzml reading failed!");
			Logger.errorException(t);

			this.error = t;
		}
	}

	@Override
	public void putBlock(MSMSBlock block) {
		try {
			blockQueue.put(block);
		} catch (InterruptedException ie) {
			Logger.errorLine("Mzml reading interrupted!");
			Logger.errorException(ie);
		}
	}
}
