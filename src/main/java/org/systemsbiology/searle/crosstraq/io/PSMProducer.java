package org.systemsbiology.searle.crosstraq.io;

public interface PSMProducer extends Runnable {
	public void putBlock(PSMBlock block);
}
