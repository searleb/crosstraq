package org.systemsbiology.searle.crosstraq.io;

import java.io.File;
import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;

public class MSMSProducerGenerator {
	public static final SimpleFilenameFilter mgf=new SimpleFilenameFilter(".mgf");
	public static final SimpleFilenameFilter mzml=new SimpleFilenameFilter(".mzml");
	
	public static MSMSProducer getProducer(ProgressIndicator progress, File f, BlockingQueue<MSMSBlock> blockQueue) {
		if (mgf.acceptFile(f)) {
			return new MGFProducer(progress, f, blockQueue);
		} else if (mzml.acceptFile(f)) {
			return new MzmlSAXProducer(progress, f, blockQueue);
		}
		throw new RuntimeException("Unsupported MSMS file type! Only MGF and MZML are supported. Sorry, I can't parse files like "+f.getName());
	}
}
