package org.systemsbiology.searle.crosstraq.io;

public interface MSMSProducer extends Runnable {
	public void putBlock(MSMSBlock block);
}
