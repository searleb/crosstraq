package org.systemsbiology.searle.crosstraq.io;

import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.utils.Logger;

public abstract class PSMConsumer implements Runnable {
	private final BlockingQueue<PSMBlock> blockQueue;

	public PSMConsumer(BlockingQueue<PSMBlock> blockQueue) {
		this.blockQueue=blockQueue;
	}

	@Override
	public void run() {
		try {
			init();
			while (true) {
				PSMBlock block=blockQueue.take();
				if (PSMBlock.POISON_BLOCK==block) break;
				processPSMBlock(block);
			}
			
		} catch (Exception e) {
			Logger.errorLine("Error writing data!");
			Logger.errorException(e);
		} finally {
			close();
		}
	}
	
	/**
	 * must be implemented
	 * @param block
	 */
	public abstract void processPSMBlock(PSMBlock block);
	
	/** 
	 * does nothing, but can be overridden to interact with thread setup
	 */
	public void init() {
		
	}

	/**
	 * does nothing, but can be overridden to interact with thread tear down.
	 * This executes in a finally even if there was an error in processing.
	 */
	public void close() {
		
	}
}
