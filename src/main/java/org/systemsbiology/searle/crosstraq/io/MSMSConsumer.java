package org.systemsbiology.searle.crosstraq.io;

import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.utils.Logger;

public abstract class MSMSConsumer implements Runnable {
	private final BlockingQueue<MSMSBlock> msBlockQueue;

	public MSMSConsumer(BlockingQueue<MSMSBlock> msBlockQueue) {
		this.msBlockQueue=msBlockQueue;
	}

	@Override
	public void run() {
		try {
			init();
			while (true) {
				MSMSBlock block=msBlockQueue.take();
				if (MSMSBlock.POISON_BLOCK==block) break;
				processMSMSBlock(block);
			}
			
		} catch (Exception e) {
			Logger.errorLine("Error writing data!");
			Logger.errorException(e);
		} finally {
			close();
		}
	}
	
	/**
	 * must be implemented
	 * @param block
	 */
	public abstract void processMSMSBlock(MSMSBlock block);
	
	/** 
	 * does nothing, but can be overridden to interact with thread setup
	 */
	public void init() {
		
	}

	/**
	 * does nothing, but can be overridden to interact with thread tear down.
	 * This executes in a finally even if there was an error in processing.
	 */
	public void close() {
		
	}
}
