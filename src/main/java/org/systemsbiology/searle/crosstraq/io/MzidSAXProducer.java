package org.systemsbiology.searle.crosstraq.io;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.SAXParserFactory;

import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.Peptide;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.structs.PostTranslationalModification;
import org.systemsbiology.searle.crosstraq.utils.Logger;
import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MzidSAXProducer extends DefaultHandler implements PSMProducer {
	private final File mzidFile;
	private final BlockingQueue<PSMBlock> psmBlockQueue;
	
	private Throwable error;
	private final StringBuilder dataSB=new StringBuilder();
	private final ArrayList<String> tagList=new ArrayList<String>();
	private final ArrayList<PeptideSpectrumMatch> psms=new ArrayList<PeptideSpectrumMatch>();
	private final ProgressIndicator progress;

	public MzidSAXProducer(ProgressIndicator progress, File mzidFile, BlockingQueue<PSMBlock> psmBlockQueue) {
		this.mzidFile=mzidFile;
		this.psmBlockQueue=psmBlockQueue;
		this.progress=progress;
	}

	@Override
	public void run() {
		try {
			final ProgressInputStream stream=new ProgressInputStream(new FileInputStream(mzidFile));
			final long length=mzidFile.length();

			stream.addChangeListener(new ChangeListener() {
				int lastUpdate=0;

				@Override
				public void stateChanged(ChangeEvent e) {
					int floor=(int)((stream.getProgress()*100L)/length);
					if (floor>lastUpdate) {
						String message="Parsing mzID "+floor+"%";
						lastUpdate=floor;
						progress.update(message, stream.getProgress()/(float)length);
					}
				}
			});

			SAXParserFactory.newInstance().newSAXParser().parse(stream, this);

			// Just for safety, ensure that if we get this far the consumer(s) of the queue will finish
			// If parse() already put a block, this will never be consumed, but it can't hurt
			putBlock(PSMBlock.POISON_BLOCK);
		} catch (Throwable t) {
			Logger.errorLine("Mzid reading failed!");
			Logger.errorException(t);

			this.error = t;
		}
	}

	@Override
	public void putBlock(PSMBlock block) {
		try {
			psmBlockQueue.put(block);
		} catch (InterruptedException ie) {
			Logger.errorLine("Mzid reading interrupted!");
			Logger.errorException(ie);
		}
	}

	public boolean hadError() {
		return null != error;
	}

	public Throwable getError() {
		return error;
	}

	HashMap<String, String> proteinAccessionsById=new HashMap<>();
	HashMap<String, Peptide> peptidesById=new HashMap<>();
	HashMap<String, String> spectraDatasetByID=new HashMap<>();
	
	String currentPeptideID;
	String currentPeptideSequence;
	ArrayList<Pair<PostTranslationalModification, Integer>> currentModList=new ArrayList<>();
	String currentModAccession;
	int currentModLocation;
	double currentModMass;
	String currentSpectrumTitle;
	String currentSpectraDataset;
	byte currentSpectrumChargeState;
	double currentSpectrumMZState;
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		dataSB.setLength(0);
		if (tagList.size()>0&&"cvParam".equalsIgnoreCase(qName)) {
			if ("Modification".equalsIgnoreCase(getPreviousElementTag())) {
				if ("UNIMOD".equalsIgnoreCase(attributes.getValue("cvRef"))) {
					currentModAccession=attributes.getValue("accession");
				} else if (attributes.getValue("accession")!=null&&currentModAccession==null) {
					Logger.errorLine("Found unexpected PTM type ["+attributes.getValue("value")+", "+attributes.getValue("accession")+"], trying to continue processing");
					//currentModAccession=attributes.getValue("accession");
				}
			} else if ("SpectrumIdentificationResult".equalsIgnoreCase(getPreviousElementTag())) {
				if ("spectrum title".equalsIgnoreCase(attributes.getValue("name"))) {
					// prefer CVParam (which occurs after the start element
					currentSpectrumTitle=attributes.getValue("value");
				}
			}
			
		} else if ("DBSequence".equalsIgnoreCase(qName)) {
			String accession=attributes.getValue("accession");
			String id=attributes.getValue("id");
			proteinAccessionsById.put(id, accession);
		} else if ("Peptide".equalsIgnoreCase(qName)) {
			// reuse this value later in the file
			currentPeptideID=attributes.getValue("id");
		} else if ("SpectrumIdentificationItem".equalsIgnoreCase(qName)) {
			// reuse this value later in the file
			currentPeptideID=attributes.getValue("peptide_ref"); 
			currentSpectrumChargeState=Byte.parseByte(attributes.getValue("chargeState")); 
			currentSpectrumMZState=Double.parseDouble(attributes.getValue("calculatedMassToCharge")); 
		} else if ("Modification".equalsIgnoreCase(qName)) {
			currentModLocation=Integer.parseInt(attributes.getValue("location"));
		} else if ("Modification".equalsIgnoreCase(qName)) {
			currentModMass=Double.parseDouble(attributes.getValue("monoisotopicMassDelta"));
		} else if ("PeptideEvidence".equalsIgnoreCase(qName)) {
			String proteinID=attributes.getValue("dBSequence_ref");
			String peptideID=attributes.getValue("peptide_ref");
			Peptide pep=peptidesById.get(peptideID);
			String accession=proteinAccessionsById.get(proteinID);
			if (pep!=null&&accession!=null) {
				pep.addProtein(accession);
			}
		} else if ("SpectrumIdentificationResult".equalsIgnoreCase(qName)) {
			currentSpectrumTitle=attributes.getValue("name");
			currentSpectraDataset=spectraDatasetByID.get(attributes.getValue("spectraData_ref"));
		} else if ("SpectraData".equalsIgnoreCase(qName)) {
			String location=attributes.getValue("location");
			String id=attributes.getValue("id");
			spectraDatasetByID.put(id, location);
		}
		
		tagList.add(qName);
	}

	private String getPreviousElementTag() {
		return tagList.get(tagList.size()-1);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		tagList.remove(tagList.size()-1);
		if ("PeptideSequence".equalsIgnoreCase(qName)) {
			currentPeptideSequence=dataSB.toString();
		} else if ("Modification".equalsIgnoreCase(qName)) {
			PostTranslationalModification ptm=PTMMap.getPTM(currentModAccession);
			if (PostTranslationalModification.nothing==ptm) {
				ptm=getUnknownPTM(currentModMass, currentModAccession);
			}
			currentModList.add(new Pair<PostTranslationalModification, Integer>(ptm, currentModLocation));
			currentModAccession=null;
			currentModLocation=-1;
			currentModMass=-1.0;
		} else if ("Peptide".equalsIgnoreCase(qName)) {
			Peptide pep=new Peptide(currentPeptideSequence, currentModList);
			peptidesById.put(currentPeptideID, pep);
			currentPeptideID=null;
			currentPeptideSequence=null;
			currentModList=new ArrayList<>();
		} else if ("SpectrumIdentificationResult".equalsIgnoreCase(qName)) {
			Peptide pep=peptidesById.get(currentPeptideID);
			if (pep==null) throw new RuntimeException("Found missing peptide id "+currentPeptideID);
			PeptideSpectrumMatch psm=new PeptideSpectrumMatch(pep, currentSpectrumMZState, currentSpectrumChargeState, currentSpectrumTitle, currentSpectraDataset, Optional.empty());
			psms.add(psm);


			if (psms.size()>1000) {
				putBlock(new PSMBlock(psms));
				psms.clear();
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		dataSB.append(ch, start, length);
	}

	@Override
	public void endDocument() throws SAXException {
		putBlock(new PSMBlock(psms));
		putBlock(PSMBlock.POISON_BLOCK);
	}

	private PostTranslationalModification getUnknownPTM(final double modMass, final String modAccession) {
		System.out.println(modAccession+" --->>"+ modMass);
		return new PostTranslationalModification(modAccession, new int[6], modMass);
	}
}
