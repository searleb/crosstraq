package org.systemsbiology.searle.crosstraq.io;

import java.util.ArrayList;

import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;

public class PSMBlock {
	public static final PSMBlock POISON_BLOCK=new PSMBlock(new ArrayList<PeptideSpectrumMatch>());

	private final ArrayList<PeptideSpectrumMatch> psms=new ArrayList<PeptideSpectrumMatch>();
	
	public PSMBlock(ArrayList<PeptideSpectrumMatch> psms) {
		this.psms.addAll(psms);
	}
	
	public ArrayList<PeptideSpectrumMatch> getPSMs() {
		return psms;
	}
}
