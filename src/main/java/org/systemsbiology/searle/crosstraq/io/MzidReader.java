package org.systemsbiology.searle.crosstraq.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.systemsbiology.searle.crosstraq.structs.FragmentScan;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.utils.Logger;
import org.systemsbiology.searle.crosstraq.utils.ProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.SubProgressIndicator;

public class MzidReader {
	public static HashMap<String, HashMap<String, PeptideSpectrumMatch>> readMzID(ProgressIndicator progress, File mzidFile) {
		BlockingQueue<PSMBlock> psmBlockQueue=new ArrayBlockingQueue<PSMBlock>(1);
		
		SubProgressIndicator sub1=new SubProgressIndicator(progress, 0.1f);
		MzidSAXProducer producer=new MzidSAXProducer(sub1, mzidFile, psmBlockQueue);
		sub1.finish("Finished reading "+mzidFile.getName());

		Thread[] threads;
		
		final HashMap<String, HashMap<String, PeptideSpectrumMatch>> psmMapByFile=new HashMap<>();
		PSMConsumer consumer=new PSMConsumer(psmBlockQueue) {
			@Override
			public void processPSMBlock(PSMBlock block) {
				for (PeptideSpectrumMatch psm : block.getPSMs()) {
					HashMap<String, PeptideSpectrumMatch> map=psmMapByFile.get(psm.getSpectraDataset());
					if (map==null) {
						map=new HashMap<>();
						psmMapByFile.put(psm.getSpectraDataset(), map);
					}
					map.put(psm.getSpectrumTitle(), psm);
				}
			}
		};
		
		Thread producerThread=new Thread(producer);
		Thread consumerThread=new Thread(consumer);
		threads=new Thread[] {producerThread, consumerThread};

		for (int i=0; i<threads.length; i++) {
			threads[i].start();
		}

		try {
			for (int i=0; i<threads.length; i++) {
				threads[i].join();
			}
			Logger.logLine("Finished reading!");

		} catch (InterruptedException ie) {
			Logger.errorLine("MzID reading interrupted!");
			Logger.errorException(ie);
		}
		
		ArrayList<String> msmsFiles=new ArrayList<>(psmMapByFile.keySet());
		Collections.sort(msmsFiles);
		for (String msmsName : msmsFiles) {
			HashMap<String, PeptideSpectrumMatch> psmsBySpectrum=psmMapByFile.get(msmsName);
			
			Logger.logLine("Trying to read "+msmsName+" ("+psmsBySpectrum.size()+" entries)");
			File msms=FileUtils.findFile(msmsName, new File[] {mzidFile});
			BlockingQueue<MSMSBlock> blockQueue=new ArrayBlockingQueue<MSMSBlock>(1);
			MSMSConsumer msmsConsumer=new MSMSConsumer(blockQueue) {
				@Override
				public void processMSMSBlock(MSMSBlock block) {
					for (FragmentScan scan : block.getFragmentScans()) {
						PeptideSpectrumMatch psm=psmsBySpectrum.get(scan.getSpectrumName());

						if (psm!=null) {
							psm.replaceMSMS(scan);
						}
					}
				}
			};

			SubProgressIndicator sub2=new SubProgressIndicator(progress, 0.9f/msmsFiles.size());
			readMS(sub2, msms, blockQueue, msmsConsumer);
			sub2.finish("Finished reading "+msms.getName());

		}
		return psmMapByFile;
	}

	public static void readMS(ProgressIndicator progress, File msms, BlockingQueue<MSMSBlock> blockQueue, MSMSConsumer consumer) {
		MSMSProducer producer=MSMSProducerGenerator.getProducer(progress, msms, blockQueue);
		
		Thread[] threads;
		Logger.logLine("Reading "+msms.getName()+" ...");
		Thread producerThread=new Thread(producer);
		Thread consumerThread=new Thread(consumer);

		threads=new Thread[] {producerThread, consumerThread};

		for (int i=0; i<threads.length; i++) {
			threads[i].start();
		}

		try {
			for (int i=0; i<threads.length; i++) {
				threads[i].join();
			}
			Logger.logLine("Finished reading "+msms.getName()+"!");

		} catch (InterruptedException ie) {
			Logger.errorLine("MSMS reading interrupted!");
			Logger.errorException(ie);
		}
	}
}