package org.systemsbiology.searle.crosstraq.io;

import java.util.ArrayList;

import org.systemsbiology.searle.crosstraq.structs.FragmentScan;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;

public class MSMSBlock {
	public static final MSMSBlock POISON_BLOCK=new MSMSBlock(new ArrayList<PrecursorScan>(), new ArrayList<FragmentScan>());
	
	private final ArrayList<PrecursorScan> precursors=new ArrayList<PrecursorScan>();
	private final ArrayList<FragmentScan> msms=new ArrayList<FragmentScan>();

	public MSMSBlock(ArrayList<PrecursorScan> precursors, ArrayList<FragmentScan> msms) {
		this.precursors.addAll(precursors);
		this.msms.addAll(msms);
	}
	
	public ArrayList<PrecursorScan> getPrecursorScans() {
		return precursors;
	}
	public ArrayList<FragmentScan> getFragmentScans() {
		return msms;
	}
}
