package org.systemsbiology.searle.crosstraq.io;

import java.util.HashMap;

import org.systemsbiology.searle.crosstraq.structs.AminoAcidConstants;
import org.systemsbiology.searle.crosstraq.structs.PostTranslationalModification;
import org.systemsbiology.searle.crosstraq.utils.IsotopicDistributionCalculator;

import uk.ac.ebi.pride.utilities.pridemod.ModReader;
import uk.ac.ebi.pride.utilities.pridemod.model.PTM;

public class PTMMap {
	private static final ModReader modReader=ModReader.getInstance();
	private static final HashMap<String, PostTranslationalModification> cache=new HashMap<>();
	
	public static void main(String[] args) {
		for (int i=1; i<100; i++) {
			String accession="UNIMOD:"+i;
			if (exists(accession)) {
				System.out.println(accession+" --> "+getName(accession)+" is ("+IsotopicDistributionCalculator.getMonoisotopicMass(getComposition(accession))+" vs "+getDeltaMass(accession)+")");
			}
		}
	}
	
	public static boolean exists(String accession) {
		return getPTM(accession)!=PostTranslationalModification.nothing;
	}

	public static String getName(String accession) {
		return getPTM(accession).getName();
	}

	public static PostTranslationalModification getPTM(String accession) {
		PostTranslationalModification ptm=cache.get(accession);
		if (ptm!=null) return ptm;
		
		synchronized (modReader) {
			// check again in case the cache has been populated by the time we get a lock
			ptm=cache.get(accession); 
			if (ptm!=null) return ptm;
			
			PTM newPTM=modReader.getPTMbyAccession(accession);
			if (newPTM==null) {
				ptm=PostTranslationalModification.nothing;
				cache.put(accession, PostTranslationalModification.nothing);
			} else {
				ptm=new PostTranslationalModification(newPTM);
				cache.put(accession, ptm);
			}
		}
		return ptm;
	}
	public static int[] getComposition(String accession) {
		return getPTM(accession).getComposition();
	}
	public static String getFormula(String accession) {
		return AminoAcidConstants.compositionToString(getComposition(accession));
		
	}
	public static double getDeltaMass(String accession) {
		return getPTM(accession).getDeltaMass();
	}
}
