package org.systemsbiology.searle.crosstraq.io;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.systemsbiology.searle.crosstraq.utils.Logger;

public class FileUtils {
	public static File findFile(String file, File[] alternateLocations) {
		return findFile(new File(file), alternateLocations);
	}
	
	public static File findFile(File file, File[] alternateLocations) {
		if (file.exists()&&file.canRead()) return file;
		
		for (File alt : alternateLocations) {
			if (alt.exists()&&alt.isDirectory()) {
				File potential=new File(alt, file.getName());
				if (potential.exists()&&potential.canRead()) return potential;		
			} else if (alt.getParentFile().exists()) {
				File potential=new File(alt.getParentFile(), file.getName());
				if (potential.exists()&&potential.canRead()) return potential;
			}
		}
		
		Logger.errorLine("Can't find file "+file.getName()+", asking user");
		String extension=FilenameUtils.getExtension(file.getName());
		
		FileDialog dialog=new FileDialog((Frame)null, "Select a "+extension.toUpperCase()+" file", FileDialog.LOAD);
		if (alternateLocations.length>0) {
			if (alternateLocations[0].isDirectory()) {
				dialog.setDirectory(alternateLocations[0].getAbsolutePath());
			} else {
				dialog.setDirectory(alternateLocations[0].getParent());
			}
		} else {
			if (file.getParentFile().exists()) {
				dialog.setDirectory(file.getParent());
			}
		}
		dialog.setFilenameFilter(new SimpleFilenameFilter(extension));
		dialog.setVisible(true);
		File[] files=dialog.getFiles();
		if (files.length>0) return files[0];
		
		throw new RuntimeException("No file selected");
	}
}
