package org.systemsbiology.searle.crosstraq.io;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.commons.io.FilenameUtils;
import org.systemsbiology.searle.crosstraq.utils.EmptyProgressIndicator;
import org.systemsbiology.searle.crosstraq.utils.Logger;

public class MS1andMS2Separator {

	public static void main(String[] args) {
		//File mzMLFile=new File("/Users/bsearle/Documents/itraq/gluc_only/K20110712_iTRAQ_GluCmix1_1-2-5.mzML");
		//File mzMLFile=new File("/Users/bsearle/Documents/itraq/gluc_in_trypsin_background/K20110715_NU_PM_iTRAQ_Prot1_fxn6_GluC_1-2-5.mzML");
		
		File dirFile=new File("/Users/bsearle/Documents/itraq/gluc_only/");
		File[] listFiles=dirFile.listFiles(MSMSProducerGenerator.mzml);
		Logger.logLine("Found "+listFiles.length+" mzML files in "+dirFile.getName());
		
		for (File mzMLFile : listFiles) {
			Logger.logLine("Processing "+mzMLFile.getName());
			String basename=FilenameUtils.removeExtension(mzMLFile.getAbsolutePath());
			File ms1MGFFile=new File(basename+".ms1.mgf");
			File ms2MGFFile=new File(basename+".ms2.mgf");
			separateMS1andMS2(mzMLFile, ms1MGFFile, ms2MGFFile);
		}
		
	}

	public static void separateMS1andMS2(File file, File ms1MGFFile, File ms2MGFFile) {
		Logger.logLine("Indexing "+file.getName()+" ...");

		BlockingQueue<MSMSBlock> blockQueue=new ArrayBlockingQueue<MSMSBlock>(1);
		MSMSProducer producer=MSMSProducerGenerator.getProducer(new EmptyProgressIndicator(), file, blockQueue);

		Thread[] threads;
		MGFWriter consumer=new MGFWriter(blockQueue, ms1MGFFile, ms2MGFFile);

		Logger.logLine("Converting "+file.getName()+" ...");
		Thread producerThread=new Thread(producer);
		Thread consumerThread=new Thread(consumer);

		threads=new Thread[] {producerThread, consumerThread};

		for (int i=0; i<threads.length; i++) {
			threads[i].start();
		}

		try {
			for (int i=0; i<threads.length; i++) {
				threads[i].join();
			}
			Logger.logLine("Finished writing "+ms1MGFFile.getName()+" and "+ms2MGFFile.getName()+"!");

		} catch (InterruptedException ie) {
			Logger.errorLine("MGF writing interrupted!");
			Logger.errorException(ie);
		}

	}
}
