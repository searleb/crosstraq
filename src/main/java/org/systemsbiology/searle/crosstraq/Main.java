package org.systemsbiology.searle.crosstraq;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.systemsbiology.searle.crosstraq.gui.CrosstalkEditorPanel;
import org.systemsbiology.searle.crosstraq.gui.FileChooserPanel;
import org.systemsbiology.searle.crosstraq.gui.JobProcessorPanel;
import org.systemsbiology.searle.crosstraq.gui.SwingJob;
import org.systemsbiology.searle.crosstraq.io.SimpleFilenameFilter;
import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.CrosstalkMatrix;
import org.systemsbiology.searle.crosstraq.structs.crosstalk.ITRAQ4PlexMatrix;

public class Main {
	private static final String[] availableMSMSOptions=new String[] {".mgf", ".mzML"};
	
	public static class ChooserPair extends JPanel {
		private static final long serialVersionUID = 1L;
		
		final FileChooserPanel mzMLChooser;
		final FileChooserPanel mzIDChooser;
		
		public ChooserPair() {
			super(new GridLayout(0, 2));
			this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3), BorderFactory.createEtchedBorder()), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
			mzIDChooser=new FileChooserPanel(null, "mzIdentML", new SimpleFilenameFilter(".mzid", ".mzidentml"), true);
			mzMLChooser=new FileChooserPanel(null, "mzML", new SimpleFilenameFilter(availableMSMSOptions), true);
			this.add(mzIDChooser);
			this.add(mzMLChooser);
		}

		private File getMZID() {
			return mzIDChooser==null?null:mzIDChooser.getFile();
		}

		private File getMZML() {
			return mzMLChooser==null?null:mzMLChooser.getFile();
		}
	}
	
	public static void main(String[] args) {
		final JFrame frame = null;
		final JDialog dialog=new JDialog(frame, "Crosstraq TMT and iTRAQ Crosstalk Processor", true);

		final ArrayList<ChooserPair> msmsFileChoosers=new ArrayList<>();
		ChooserPair fileChooser=new ChooserPair();
		msmsFileChoosers.add(fileChooser);
		
		final JPanel choosers=new JPanel();
		choosers.setLayout(new BoxLayout(choosers, BoxLayout.Y_AXIS));
		choosers.add(fileChooser);
		
		JPanel organizer=new JPanel(new BorderLayout());
		organizer.add(choosers, BorderLayout.NORTH);
		//JScrollPane scrollPane = new JScrollPane(organizer, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); 
		
		JPanel buttons=new JPanel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER));

		JButton addButton=new JButton("Add File");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFile(msmsFileChoosers, choosers);
			}
		});
		buttons.add(addButton);

		JButton resetButton=new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reset(msmsFileChoosers, choosers);
			}
		});
		buttons.add(resetButton);
		
		JButton okButton=new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runJobs(frame, dialog, msmsFileChoosers);
			}
		});
		
		buttons.add(okButton);
		JButton cancelButton=new JButton("Quit");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				quit(dialog);
			}
		});
		buttons.add(cancelButton);
		
		JPanel mainpane=new JPanel(new BorderLayout());
		mainpane.add(organizer, BorderLayout.CENTER);
		mainpane.add(buttons, BorderLayout.SOUTH);
		organizer.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), BorderFactory.createTitledBorder("Files to process:")));
		
		JMenuBar bar=new JMenuBar();
		JMenu fileMenu=new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		bar.add(fileMenu);

		JMenuItem addFile=new JMenuItem("Add File");
		addFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addFile(msmsFileChoosers, choosers);
			}
		});
		addFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(addFile);

		JMenuItem reset=new JMenuItem("Reset");
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reset(msmsFileChoosers, choosers);
			}
		});
		reset.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(reset);

		JMenuItem startJobs=new JMenuItem("Start Jobs");
		startJobs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				runJobs(frame, dialog, msmsFileChoosers);
			}
		});
		startJobs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(startJobs);

		JMenuItem quit=new JMenuItem("Quit");
		quit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				quit(dialog);
			}
		});
		quit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		fileMenu.add(quit);

		dialog.setJMenuBar(bar);
		dialog.getContentPane().add(mainpane, BorderLayout.CENTER);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.pack(); 
		dialog.setSize(new Dimension(900, 600));
		dialog.setVisible(true);
	}

	private static void addFile(final ArrayList<ChooserPair> msmsFileChoosers, final JPanel choosers) {
		ChooserPair fileChooser=new ChooserPair();
		msmsFileChoosers.add(fileChooser);
		choosers.add(fileChooser, choosers.getComponentCount());
		choosers.revalidate();
		choosers.repaint();
	}

	private static void runJobs(final JFrame frame, final JDialog dialog,
			final ArrayList<ChooserPair> msmsFileChoosers) {
		ArrayList<ImmutablePair<File, File>> msmsFiles=new ArrayList<>();
		for (ChooserPair msms : msmsFileChoosers) {
			File mzid=msms.getMZID();
			File mzml=msms.getMZML();
			
			if (mzid!=null&&mzid.exists()&&mzml!=null&&mzml.exists()) {
				msmsFiles.add(new ImmutablePair<File, File>(mzid, mzml));
			}
		}

		if (msmsFiles.size()>0) {
			Optional<Pair<CrosstalkMatrix, Boolean>> matrix=CrosstalkEditorPanel.askUserForCrosstalk(dialog, new ITRAQ4PlexMatrix());
			if (!matrix.isPresent()) return;
			boolean usePurityCorrection=matrix.get().getSecond();
			
			ArrayList<SwingJob> jobs=new ArrayList<>();
			for (ImmutablePair<File, File> pair : msmsFiles) {
				jobs.add(new ReadMZID(pair.left, pair.right, matrix.get().getFirst(), usePurityCorrection));
			}
			JobProcessorPanel.processJobs(dialog, jobs);
			
		} else {
			JOptionPane.showMessageDialog(frame, "You must specify mzIdentML and mzML files!", "Incomplete options!", JOptionPane.WARNING_MESSAGE, null);
		}
	}

	private static void quit(final JDialog dialog) {
		dialog.setVisible(false);
		dialog.dispose();
	}

	private static void reset(final ArrayList<ChooserPair> msmsFileChoosers, final JPanel choosers) {
		choosers.removeAll();
		msmsFileChoosers.clear();
		
		addFile(msmsFileChoosers, choosers);
	}

	private static class ReadMZID extends SwingJob {
		private final File mzid;
		private final File mzml;
		private final CrosstalkMatrix matrix;
		private final boolean usePurityCorrection;
		
		public ReadMZID(File mzid, File mzml, CrosstalkMatrix matrix, boolean usePurityCorrection) {
			super();
			this.mzid = mzid;
			this.mzml = mzml;
			this.matrix=matrix;
			this.usePurityCorrection=usePurityCorrection;
		}

		@Override
		public void runJob() throws Exception {
			CrosstraqWorkflow.processMzID(getProgressIndicator(), mzid, mzml, (byte)-1, null, matrix, usePurityCorrection);
		}

		@Override
		public String getJobTitle() {
			return mzid.getName();
		}
		
	}
}
