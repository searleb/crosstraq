package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class TMT11PlexMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"TMT_126", "TMT_127N", "TMT_127C", "TMT_128N", "TMT_128C", "TMT_129N", "TMT_129C", "TMT_130N", "TMT_130C", "TMT_131N", "TMT_131C"};
	private static final double[] ION_MZS=new double[] {126.127726, 127.124761, 127.131081, 128.128116, 128.134436, 129.131471, 129.13779, 130.134825, 130.141145, 131.13818, 131.144499};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 4.69f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.4f, 0.0f, 0.0f, 6.50f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.2f, 0.0f, 0.0f, 4.60f, 0.3f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.9f, 0.0f, 0.0f, 4.70f, 0.2f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.1f, 0.0f, 0.53f, 0.0f, 0.0f, 2.59f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.73f, 0.0f, 0.0f, 2.49f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 1.3f, 0.0f, 0.0f, 2.50f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 1.2f, 0.0f, 0.0f, 2.80f, 2.7f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.1f, 0.0f, 2.90f, 0.0f, 0.0f, 2.90f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 2.36f, 0.0f, 0.0f, 1.43f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 2.36f, 0.0f, 0.0f, 1.43f, 0.0f, 0.0f}, 0.01f)}; // fabricated, assuming similar to 10plex
	private static final String NAME="TMT-11plex";
	public String getName() {
		return NAME;
	}
		
	private final float[][] inverseCorrectionMatrix;
	
	public static void main(String[] args) {
		int n=1000000;
		
		for (int num=0; num<10; num++) {
			CrosstalkMatrix matrix=new TMT11PlexMatrix();
			float[][] dataset=new float[n][];
			for (int i = 0; i < dataset.length; i++) {
				float[] f=new float[matrix.getIonNames().length];
				for (int j = 0; j < f.length; j++) {
					f[j]=(float)Math.random();
				}
				dataset[i]=f;
			}
			
			long time=System.currentTimeMillis();
			for (int i=0; i<dataset.length; i++) {
				matrix.correct(dataset[i]);
			}
			long inverseTime = System.currentTimeMillis()-time;
			
			time=System.currentTimeMillis();
			for (int i=0; i<dataset.length; i++) {
				matrix.correctUsingCramerMethod(dataset[i]);
			}
			long cramerTime = System.currentTimeMillis()-time;
			System.out.println((cramerTime/(float)inverseTime)+"\t"+inverseTime+"\t"+cramerTime);
		}
	}

	public TMT11PlexMatrix() {
		this(DEFAULT_FACTORS);
	}

	private static float[][] get11plexCorrectionMatrix(float[][] correctionFactors) {
		float[][] matrix=new float[correctionFactors.length][];
		for (int i=0; i<matrix.length; i++) {
			matrix[i]=new float[correctionFactors.length];
			matrix[i][i]=1.0f-General.sum(correctionFactors[i]);
			if (i>=4) matrix[i][i-4]=correctionFactors[i][0];
			if (i>=3) matrix[i][i-3]=correctionFactors[i][1];
			if (i>=2) matrix[i][i-2]=correctionFactors[i][2];
			if (i>=1) matrix[i][i-1]=correctionFactors[i][3];
			if (i<correctionFactors.length-1) matrix[i][i+1]=correctionFactors[i][4];
			if (i<correctionFactors.length-2) matrix[i][i+2]=correctionFactors[i][5];
			if (i<correctionFactors.length-3) matrix[i][i+3]=correctionFactors[i][6];
			if (i<correctionFactors.length-4) matrix[i][i+4]=correctionFactors[i][7];
		}
		return matrix;
	}
	
	public TMT11PlexMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, get11plexCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new TMT11PlexMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
