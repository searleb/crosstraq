package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class ITRAQ8PlexWithYMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"iTr-113", "iTr-114", "iTr-115", "iTr-116", "iTr-117", "iTr-118", "iTr-119", "F-Immonium", "iTr-121"};
	private static final double[] ION_MZS=new double[] {113.107, 114.111, 115.108, 116.111, 117.115, 118.111, 119.115, 120.0813, 121.122};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 2.5f, 3.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 1.0f, 5.9f, 0.2f}, 0.01f),
		General.multiply(new float[] {0.0f, 2.0f, 5.6f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 3.0f, 4.5f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 4.0f, 3.5f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 2.0f, 3.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 2.0f, 4.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 8.4f, 0.3f}, 0.01f), // from phenylalanine immonium ion elemental composition C8N1H10 (mono=0.913242009, +1=0.084018265, +2=0.002739726 from https://www.sisweb.com/mstools/isotope.htm) 
		General.multiply(new float[] {0.1f, 2.0f, 3.0f, 0.1f}, 0.01f)};
		private static final String NAME="iTRAQ-8plex";
		public String getName() {
			return NAME;
		}
	
	private final float[][] inverseCorrectionMatrix;

	public static void main(String[] args) {
		CrosstalkMatrix matrix=new ITRAQ8PlexWithYMatrix();
		System.out.println(matrix);
		System.out.println();
		System.out.println(General.toString(CrosstalkMatrix.getCorrectionMatrix(matrix.get2n2CorrectionFactors()), "\t"));
		System.out.println();
		System.out.println(General.toString(matrix.correct(new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f})));
		System.out.println(General.toString(matrix.correctUsingCramerMethod(new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f})));
	}

	public ITRAQ8PlexWithYMatrix() {
		this(DEFAULT_FACTORS);
	}
	
	public ITRAQ8PlexWithYMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, CrosstalkMatrix.getCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new ITRAQ8PlexWithYMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
