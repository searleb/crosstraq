package org.systemsbiology.searle.crosstraq.structs;

public class Peak implements HasRetentionTime, HasMZ {
	private final float rtInSec;
	private final double mz;
	private final float intensity;

	public Peak(float rtInSec, double mz, float intensity) {
		this.rtInSec = rtInSec;
		this.mz = mz;
		this.intensity=intensity;
	}

	@Override
	public double getMZ() {
		return mz;
	}

	@Override
	public float getRetentionTimeInSec() {
		return rtInSec;
	}

	public float getIntensity() {
		return intensity;
	}
}
