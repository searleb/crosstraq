package org.systemsbiology.searle.crosstraq.structs;

import java.util.Comparator;

public class MZComparator implements Comparator<HasMZ> {
	public static final MZComparator comparator=new MZComparator();
	
	@Override
	public int compare(HasMZ o1, HasMZ o2) {
		if (o1==null&&o2==null) return 0;
		else if (o1==null) return 1;
		else if (o2==null) return -1;
		
		return Double.compare(o1.getMZ(), o2.getMZ());
	}

	public static HasMZ getComparable(final float mz) {
		return new HasMZ() {
			@Override
			public double getMZ() {
				return mz;
			}
		};
	}
}
