package org.systemsbiology.searle.crosstraq.structs;

import java.util.Optional;

public class PeptideSpectrumMatch implements HasRetentionTime, HasMZ {
	private final Peptide peptide;
	private final double computedMz;
	private final byte charge;
	private final String spectrumTitle;
	private final String spectraDataset;
	
	// mutable, be careful!
	private Optional<FragmentScan> msms;

	public PeptideSpectrumMatch(Peptide peptide, double computedMz, byte charge, String spectrumTitle, String spectraDataset, Optional<FragmentScan> msms) {
		this.peptide=peptide;
		this.computedMz=computedMz;
		this.charge=charge;
		this.msms=msms;
		this.spectrumTitle=spectrumTitle;
		this.spectraDataset=spectraDataset;
	}
	
	public void replaceMSMS(FragmentScan msms) {
		this.msms=Optional.of(msms);
	}

	public Peptide getPeptide() {
		return peptide;
	}

	public double getComputedMz() {
		return computedMz;
	}
	
	@Override
	public double getMZ() {
		return computedMz;
	}

	public byte getCharge() {
		return charge;
	}
	
	public String getSpectrumTitle() {
		return spectrumTitle;
	}
	
	public String getSpectraDataset() {
		return spectraDataset;
	}

	public Optional<FragmentScan> getMSMS() {
		return msms;
	}
	
	@Override
	public float getRetentionTimeInSec() {
		if (msms.isPresent()) return msms.get().getRetentionTimeInSec();
		return 0.0f;
	}
}
