package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.Log;
import org.systemsbiology.searle.crosstraq.utils.QuickMedian;

public class CrosstalkAccuracyWithNoise {
	public static void main(String[] args) {
		CrosstalkMatrix matrix=new ITRAQ4PlexMatrix();
		int numberOfSamples=1000;
		
		for (int n = 0; n < 200; n++) {
			float noiseLevel=n/1000f;
			
			float[] samples=new float[numberOfSamples];
			for (int s = 0; s < numberOfSamples; s++) {
				float[] data=new float[4];
				for (int i = 0; i < data.length; i++) {
					data[i]=(float)Math.random();
				}
				data=new float[] {1, 3, 10, 0};
				data=General.normalize(data);
				
				float[] convoluted = convolute(data, matrix);
				for (int i = 0; i < convoluted.length; i++) {
					// normalize makes the average signal 1/4, so equivalent noise is 10/40
					float noise = getPoisson(10)/40f; 
					convoluted[i]+=noiseLevel*noise;
				}
				
				//float[] corrected=convoluted;
				//float[] corrected=matrix.correct(convoluted);
				float[] corrected=matrix.correctUsingNNLS(convoluted);
				
				float[] origRatios=Log.protectedLog2(General.divide(data, data[0]));
				float[] correctedRatios=Log.protectedLog2(General.divide(corrected, corrected[0]));
				samples[s]=General.euclideanDistance(data, corrected);
			}
			System.out.println(noiseLevel+", "+QuickMedian.median(samples));
		}
	}
	
	public static int getPoisson(double lambda) {
		  double L = Math.exp(-lambda);
		  double p = 1.0;
		  int k = 0;

		  do {
		    k++;
		    p *= Math.random();
		  } while (p > L);

		  return k - 1;
		}

	private static float[] convolute(float[] data, CrosstalkMatrix matrix) {
		float[] convoluted=new float[data.length];
		float[][] factors=matrix.get2n2CorrectionFactors();
		for (int i = 0; i < data.length; i++) {
			if (i>0) convoluted[i-1]+=data[i]*factors[i][1];
			if (i<data.length-1) convoluted[i+1]+=data[i]*factors[i][2];

			if (i>1) convoluted[i-2]+=data[i]*factors[i][0];
			if (i<data.length-2) convoluted[i+2]+=data[i]*factors[i][3];

			convoluted[i]+=data[i]*(1.0f-factors[i][0]-factors[i][1]-factors[i][2]-factors[i][3]);
		}
		return convoluted;
	}

}
