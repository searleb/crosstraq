package org.systemsbiology.searle.crosstraq.structs.integration;

import org.systemsbiology.searle.crosstraq.structs.HasMZ;

public class Ion implements HasMZ, Comparable<Ion> {
	private final String peptide;
	private final byte charge;
	private final byte isotope;
	private final double mz;
	public Ion(String peptide, byte charge, byte isotope, double mz) {
		this.peptide = peptide;
		this.charge = charge;
		this.isotope = isotope;
		this.mz = mz;
	}
	public String getPeptide() {
		return peptide;
	}
	public byte getCharge() {
		return charge;
	}
	public byte getIsotope() {
		return isotope;
	}
	
	@Override
	public double getMZ() {
		return mz;
	}
	
	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder(peptide);
		sb.append("+");
		sb.append(charge);
		sb.append("H");
		if (isotope>0) {
			sb.append("+");
			sb.append(isotope);
		} else if (isotope<0) {
			sb.append(isotope);
		}
		return sb.toString();
	}
	@Override
	public int compareTo(Ion o) {
		if (o==null) return 1;
		
		int c=charge-o.charge;
		if (c!=0) return c;
		c=isotope-o.isotope;
		if (c!=0) return c;
		c=Double.compare(mz,o.mz);
		if (c!=0) return c;
		c=peptide.compareTo(o.peptide);
		if (c!=0) return c;
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Ion) {
			return this.compareTo((Ion)obj)==0;
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		// most of the time we're not comparing between peptides, so keep this cheap to calculate
		return charge+16807*isotope;
	}
}
