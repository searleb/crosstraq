package org.systemsbiology.searle.crosstraq.structs;

public interface HasMZ {

	double getMZ();

}