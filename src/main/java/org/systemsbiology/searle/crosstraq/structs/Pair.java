package org.systemsbiology.searle.crosstraq.structs;

public class Pair <J,K> {
	private final J left;
	private final K right;

	public Pair(J left, K right) {
		this.left = left;
		this.right = right;
	}

	public J getLeft() {
		return left;
	}

	public K getRight() {
		return right;
	}

	public J getFirst() {
		return left;
	}

	public K getSecond() {
		return right;
	}
}
