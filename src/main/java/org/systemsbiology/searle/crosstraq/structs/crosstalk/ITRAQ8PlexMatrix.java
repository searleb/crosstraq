package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class ITRAQ8PlexMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"iTr-113", "iTr-114", "iTr-115", "iTr-116", "iTr-117", "iTr-118", "iTr-119", "iTr-121"};
	private static final double[] ION_MZS=new double[] {113.107, 114.111, 115.108, 116.111, 117.115, 118.111, 119.115, 121.122};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 2.5f, 3.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 1.0f, 5.9f, 0.2f}, 0.01f),
		General.multiply(new float[] {0.0f, 2.0f, 5.6f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 3.0f, 4.5f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 4.0f, 3.5f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 2.0f, 3.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 2.0f, 4.0f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 2.0f, 3.0f, 0.1f}, 0.01f)};
		private static final String NAME="iTRAQ-8plex";
		public String getName() {
			return NAME;
		}
	
	private final float[][] inverseCorrectionMatrix;

	public static void main(String[] args) {
		float[][] factors={{0.0f,	0.0f,	0.0689f,	0.0022f},
			{0.0f,	0.0094f,	0.059f,	0.0016f},
				{0.0f,	0.0188f,	0.049f,	0.001f},
				{0.0f,	0.0282f,	0.039f,	7.0E-4f},
				{6.0E-4f,	0.0377f,	0.0288f,	0.0f},
				{9.0E-4f,	0.0471f,	0.0188f,	0.0f},
				{0.0014f,	0.0566f,	0.0087f,	0.0f},
				{0.0027f,	0.0744f,	0.0018f,	0.0f}};
				
		CrosstalkMatrix matrix=new ITRAQ8PlexMatrix(factors);
		System.out.println(matrix);
		System.out.println();
		System.out.println(General.toString(get8plexCorrectionMatrix(matrix.get2n2CorrectionFactors()), "\t"));
		System.out.println();
		System.out.println(General.toString(matrix.correct(new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f})));
		System.out.println(General.toString(matrix.correctUsingCramerMethod(new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f})));
	}

	public ITRAQ8PlexMatrix() {
		this(DEFAULT_FACTORS);
	}

	protected static float[][] get8plexCorrectionMatrix(float[][] correctionFactors) {
		float[][] matrix=new float[correctionFactors.length][];
		for (int i=0; i<matrix.length; i++) {
			matrix[i]=new float[correctionFactors.length];
			matrix[i][i]=1.0f-General.sum(correctionFactors[i]);
			if (i>=2) matrix[i][i-2]=correctionFactors[i][0];
			if (i>=1) matrix[i][i-1]=correctionFactors[i][1];
			if (i<correctionFactors.length-1) matrix[i][i+1]=correctionFactors[i][2];
			if (i<correctionFactors.length-2) matrix[i][i+2]=correctionFactors[i][3];
		}
		
		// hack to deal with missing 120 ion
		matrix[5][7]=0.0f;
		matrix[6][7]=0.0f;
		matrix[7][6]=0.0f;
		return matrix;
	}
	
	public ITRAQ8PlexMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, get8plexCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new ITRAQ8PlexMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
