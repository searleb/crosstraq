package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class ITRAQ4PlexMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"iTr-114", "iTr-115", "iTr-116", "iTr-117"};
	private static final double[] ION_MZS=new double[] {114.111, 115.108, 116.112, 117.115};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 1.0f, 5.9f, 0.2f}, 0.01f),
		General.multiply(new float[] {0.0f, 2.0f, 5.6f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.0f, 3.0f, 4.5f, 0.1f}, 0.01f),
		General.multiply(new float[] {0.1f, 4.0f, 3.5f, 0.1f}, 0.01f)};
	private static final String NAME="iTRAQ-4plex";
	public String getName() {
		return NAME;
	}
	
	private final float[][] inverseCorrectionMatrix;
	
	public static void main(String[] args) {
		float plus1=0.05f;
		float plus2=0.00125f;
		float minus1=0.025f;
		float minus2=0.00025f;
		float[][] altfactors=new float[][] {
			new float[] {minus2, minus1, plus1, plus2},
			new float[] {minus2, minus1, plus1, plus2},
			new float[] {minus2, minus1, plus1, plus2},
			new float[] {minus2, minus1, plus1, plus2}
		};
		CrosstalkMatrix matrix=new ITRAQ4PlexMatrix();
		float[][] factors=matrix.get2n2CorrectionFactors();
		
		float[] data=new float[] {1, 3, 10, 0};
		data=new float[] {1, 2, 5, 0};
		float[] convoluted=new float[data.length];
		
		for (int i = 0; i < data.length; i++) {
			if (i>0) convoluted[i-1]+=data[i]*factors[i][1];
			if (i<data.length-1) convoluted[i+1]+=data[i]*factors[i][2];

			if (i>1) convoluted[i-2]+=data[i]*factors[i][0];
			if (i<data.length-2) convoluted[i+2]+=data[i]*factors[i][3];
			
			convoluted[i]+=data[i]*(1.0f-factors[i][0]-factors[i][1]-factors[i][2]-factors[i][3]);
		}
		
		float[] deviation=new float[convoluted.length];
		for (int i = 0; i < deviation.length; i++) {
			deviation[i]=(convoluted[i]-data[i])/Math.max(convoluted[i], data[i]);
		}

		System.out.println(General.toString(data));
		System.out.println(General.toString(convoluted));
		System.out.println(General.toPercentString(deviation));
		System.out.println(General.toString(matrix.correct(convoluted)));
		System.out.println(General.euclideanDistance(data, convoluted)+" --> "+General.euclideanDistance(data, matrix.correct(convoluted)));
	}

	public ITRAQ4PlexMatrix() {
		this(DEFAULT_FACTORS);
	}
	
	public ITRAQ4PlexMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, CrosstalkMatrix.getCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new ITRAQ4PlexMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
