package org.systemsbiology.searle.crosstraq.structs;

public interface Spectrum extends HasRetentionTime {
	public String getSpectrumName();
	public double getPrecursorMz();
	public double[] getMassArray();
	public float[] getIntensityArray();
	public float getTIC();
	public int getSpectrumIndex();
}
