package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import gnu.trove.list.array.TFloatArrayList;

public class CrosstalkTimeTrials {
	public static void main(String[] args) {
		int n=1000000;
		int count=100;

		//tmt6\titraq8\ttmt10\ttmt11");
		System.out.print("itraq4\t");
		float[] itraq4times=runTrial(n, count, new ITRAQ4PlexMatrix());
		System.out.print("tmt6\t");
		float[] tmt6times=runTrial(n, count, new TMT6PlexMatrix());
		System.out.print("itraq8\t");
		float[] itraq8times=runTrial(n, count, new ITRAQ8PlexWithYMatrix());
		System.out.print("tmt10\t");
		float[] tmt10times=runTrial(n, count, new TMT10PlexMatrix());
		System.out.print("tmt11\t");
		float[] tmt11times=runTrial(n, count, new TMT11PlexMatrix());
		System.out.println();
		
		for (int i = 0; i < tmt11times.length; i++) {
			System.out.println(itraq4times[i]+"\t"+tmt6times[i]+"\t"+itraq8times[i]+"\t"+tmt10times[i]+"\t"+tmt11times[i]);
		}
	}

	private static float[] runTrial(int n, int count, CrosstalkMatrix seed) {
		TFloatArrayList times=new TFloatArrayList();
		for (int num=0; num<count; num++) {
			CrosstalkMatrix matrix=seed.generateNewMatrix(seed.get2n2CorrectionFactors());
			float[][] dataset=new float[n][];
			for (int i = 0; i < dataset.length; i++) {
				float[] f=new float[matrix.getIonNames().length];
				for (int j = 0; j < f.length; j++) {
					f[j]=(float)Math.random();
				}
				dataset[i]=f;
			}
			
			long time=System.currentTimeMillis();
			for (int i=0; i<dataset.length; i++) {
				matrix.correct(dataset[i]);
			}
			long inverseTime = System.currentTimeMillis()-time;
			
			time=System.currentTimeMillis();
			for (int i=0; i<dataset.length/100; i++) {
				//matrix.correctUsingCramerMethod(dataset[i]);
				matrix.correctUsingNNLS(dataset[i]);
			}
			long cramerTime = 100*(System.currentTimeMillis()-time);
			
			times.add((cramerTime/(float)inverseTime));
		}
		return times.toArray();
	}
}
