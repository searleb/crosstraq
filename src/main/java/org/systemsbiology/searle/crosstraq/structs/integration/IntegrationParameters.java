package org.systemsbiology.searle.crosstraq.structs.integration;

import org.systemsbiology.searle.crosstraq.structs.MassTolerance;

public class IntegrationParameters {
	private static final float MINIMUM_TROUGH_RT_PERCENTAGE = 0.10f;
	private static final float MINIMUM_TROUGH_INTENSITY_PERCENTAGE = 0.5f;
	private static final float MINIMUM_BOUNDARY_INTENSITY_PERCENTAGE = 0.05f;
	private static final float INTEGRATION_TIME_WINDOW_IN_SEC = 300f;
	private static final MassTolerance TOLERANCE = new MassTolerance(25);

	public IntegrationParameters() {
	}
	
	public float getMinimumBoundaryIntensityPercentage() {
		return MINIMUM_BOUNDARY_INTENSITY_PERCENTAGE;
	}
	public float getMinimumTroughIntensityPercentage() {
		return MINIMUM_TROUGH_INTENSITY_PERCENTAGE;
	}
	public float getMinimumTroughRtPercentage() {
		return MINIMUM_TROUGH_RT_PERCENTAGE;
	}
	public float getIntegrationTimeWindowInSec() {
		return INTEGRATION_TIME_WINDOW_IN_SEC;
	}
	
	public MassTolerance getTolerance() {
		return TOLERANCE;
	}
}
