package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class TMT6PlexMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"TMT-126", "TMT-127", "TMT-128", "TMT-129", "TMT-130", "TMT-131"};
	private static final double[] ION_MZS=new double[] {126.127726, 127.124761, 128.134436, 129.131471, 130.141145, 131.138180};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 0.0f, 6.1f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.5f, 6.7f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 1.1f, 4.2f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 1.7f, 4.1f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 1.6f, 2.1f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.2f, 3.2f, 2.8f, 0.0f}, 0.01f)};
	private static final String NAME="TMT-6plex";
	public String getName() {
		return NAME;
	}
	
	private final float[][] inverseCorrectionMatrix;
	
	public static void main(String[] args) {
		CrosstalkMatrix matrix=new TMT6PlexMatrix();
		System.out.println(matrix);
		System.out.println();
		System.out.println(General.toString(matrix.correct(new float[] {1f, 1f, 1f, 1f, 1f, 1f})));
	}

	public TMT6PlexMatrix() {
		this(DEFAULT_FACTORS);
	}
	
	public TMT6PlexMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, CrosstalkMatrix.getCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new TMT6PlexMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
