package org.systemsbiology.searle.crosstraq.structs;

import uk.ac.ebi.pride.utilities.pridemod.model.PTM;

public class PostTranslationalModification {
	public static final PostTranslationalModification nothing=new PostTranslationalModification("nothing", new int[6], 0.0);
	private final String name;
	private final int[] composition;
	private final double deltaMass;

	public PostTranslationalModification(PTM ptm) {
		name=ptm.getName();
		composition=AminoAcidConstants.parseAminoAcidProportions(ptm.getFormula());
		deltaMass=ptm.getMonoDeltaMass();
	}
	
	public PostTranslationalModification(String name, int[] composition, double deltaMass) {
		this.name=name;
		this.composition=composition;
		this.deltaMass=deltaMass;
	}
	
	public int[] getComposition() {
		return composition;
	}

	public double getDeltaMass() {
		return deltaMass;
	}

	public String getName() {
		return name;
	}
}
