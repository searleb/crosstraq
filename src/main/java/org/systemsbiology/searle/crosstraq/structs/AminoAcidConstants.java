package org.systemsbiology.searle.crosstraq.structs;

import java.util.StringTokenizer;

import org.systemsbiology.searle.crosstraq.utils.MassConstants;

import gnu.trove.map.hash.TCharDoubleHashMap;
import gnu.trove.map.hash.TCharObjectHashMap;

public class AminoAcidConstants {
	public static final char[] AAs="ARNDCEQGHLIKMFPSTWYV".toCharArray();
	
	// ordered by H C O N S P
	private static final TCharObjectHashMap<int[]> atomicComposition=new TCharObjectHashMap<int[]>();
	private static final TCharDoubleHashMap massesByAA=new TCharDoubleHashMap();

	static {
		atomicComposition.put('A', new int[] {5, 3, 1, 1, 0, 0});
		atomicComposition.put('C', new int[] {5, 3, 1, 1, 1, 0});
		atomicComposition.put('D', new int[] {5, 4, 3, 1, 0, 0});
		atomicComposition.put('E', new int[] {7, 5, 3, 1, 0, 0});
		atomicComposition.put('F', new int[] {9, 9, 1, 1, 0, 0});
		atomicComposition.put('G', new int[] {3, 2, 1, 1, 0, 0});
		atomicComposition.put('H', new int[] {7, 6, 1, 3, 0, 0});
		atomicComposition.put('I', new int[] {11, 6, 1, 1, 0, 0});
		atomicComposition.put('K', new int[] {12, 6, 1, 2, 0, 0});
		atomicComposition.put('L', new int[] {11, 6, 1, 1, 0, 0});
		atomicComposition.put('M', new int[] {9, 5, 1, 1, 1, 0});
		atomicComposition.put('N', new int[] {6, 4, 2, 2, 0, 0});
		atomicComposition.put('P', new int[] {7, 5, 1, 1, 0, 0});
		atomicComposition.put('Q', new int[] {8, 5, 2, 2, 0, 0});
		atomicComposition.put('R', new int[] {12, 6, 1, 4, 0, 0});
		atomicComposition.put('S', new int[] {5, 3, 2, 1, 0, 0});
		atomicComposition.put('T', new int[] {7, 4, 2, 1, 0, 0});
		atomicComposition.put('V', new int[] {9, 5, 1, 1, 0, 0});
		atomicComposition.put('W', new int[] {10, 11, 1, 2, 0, 0});
		atomicComposition.put('Y', new int[] {9, 9, 2, 1, 0, 0});

		massesByAA.put('A', 71.037114);
		massesByAA.put('R', 156.101111);
		massesByAA.put('N', 114.042927);
		massesByAA.put('D', 115.026943);
		massesByAA.put('C', 103.009185);
		massesByAA.put('E', 129.042593);
		massesByAA.put('Q', 128.058578);
		massesByAA.put('G', 57.021464);
		massesByAA.put('H', 137.058912);
		massesByAA.put('L', 113.084064);
		massesByAA.put('I', 113.084064);
		massesByAA.put('K', 128.094963);
		massesByAA.put('M', 131.040485);
		massesByAA.put('F', 147.068414);
		massesByAA.put('P', 97.052764);
		massesByAA.put('S', 87.032028);
		massesByAA.put('T', 101.047679);
		massesByAA.put('W', 186.079313);
		massesByAA.put('Y', 163.06332);
		massesByAA.put('V', 99.068414);
	}
	
	public static double getMass(char aa) {
		return massesByAA.get(aa);
	}
	
	public static double getMass(String sequence) {
		double total=0.0;
		for (int i=0; i<sequence.length(); i++) {
			char c=sequence.charAt(i);

			if (c=='[') {
				final StringBuilder sb=new StringBuilder();
				while (true) {
					i++;
					c=sequence.charAt(i);
					if (c==']') {
						break; 
					} else {
						sb.append(c);
					}
				}
				final double modMass = Double.parseDouble(sb.toString());
				total += modMass;
			}
			total+=getMass(c);
		}
		return total;
	}
	
	public static double getChargedMass(String modSeq, byte charge) {
		double mass=getMass(modSeq)+MassConstants.oh2;
		return (mass+MassConstants.protonMass*charge)/charge;
	}

	public static double getChargedIsotopeMass(String modSeq, byte charge, byte isotope) {
		return MassConstants.getChargedIsotopeMass(getChargedMass(modSeq, charge), charge, isotope);
	}

	public static int[] getAminoAcidProportions(char c) {
		int[] is=atomicComposition.get(c);
		return is;
	}
	
	public static int[] getPeptideBaseProportions() {
		int[] base=new int[] {2, 0, 1, 0, 0, 0}; 
		return base;
	}
	
	/**
	 * note this doesn't know anything about nonstandard elements (H C O N S P)
	 * TODO figure out a way to support more elements
	 * @param s
	 * @return
	 */
	public static int[] parseAminoAcidProportions(String s) {
		// e.g. H(4) 13C(4) O(3)
		StringTokenizer st=new StringTokenizer(s, " ");
		
		int[] composition=new int[6];
		while (st.hasMoreTokens()) {
			String token=st.nextToken();
			
			int index=0;
			
			// trim off elemental composition (we're ignoring this), TODO this could be better
			for (; index<token.length(); index++) {
				char c=token.charAt(index);
				if (!Character.isDigit(c)) break;
			}

			StringBuilder element=new StringBuilder();
			for (; index<token.length(); index++) {
				char c=token.charAt(index);
				if (Character.isAlphabetic(c)) {
					element.append(c);
				} else {
					break;
				}
			}

			StringBuilder count=new StringBuilder();
			for (; index<token.length(); index++) {
				char c=token.charAt(index);
				if (Character.isDigit(c)||'-'==c) { // support negative values
					count.append(c);
				} else if (count.length()>0) {
					break;
				}
			}

			int elementIndex;
			if (element.length()==1) {
				if ('H'==element.charAt(0)) elementIndex=0;
				else if ('C'==element.charAt(0)) elementIndex=1;
				else if ('O'==element.charAt(0)) elementIndex=2;
				else if ('N'==element.charAt(0)) elementIndex=3;
				else if ('S'==element.charAt(0)) elementIndex=4;
				else if ('P'==element.charAt(0)) elementIndex=5;
				else continue;
			} else {
				continue;
			}

			// if there's no info then assume it's empty
			int elementCount=count.length()==0?1:Integer.parseInt(count.toString());
			composition[elementIndex]=elementCount;
		}
		return composition;
	}
	
	public static String compositionToString(int[] composition) {
		StringBuilder sb=new StringBuilder();
		for (int i=0; i<composition.length; i++) {
			if (composition[i]>0) {
				if (sb.length()>0) sb.append(" ");
				if (i==0) sb.append("H(");
				else if (i==1) sb.append("C(");
				else if (i==2) sb.append("O(");
				else if (i==3) sb.append("N(");
				else if (i==4) sb.append("S(");
				else if (i==5) sb.append("P(");
				else continue;
				sb.append(composition[i]);
				sb.append(")");
			}
		}
		return sb.toString();
	}
}
