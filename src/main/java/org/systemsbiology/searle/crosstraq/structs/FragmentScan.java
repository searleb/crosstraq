package org.systemsbiology.searle.crosstraq.structs;

import java.util.ArrayList;
import java.util.Collections;

import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.Range;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;

//@Immutable
public class FragmentScan implements Comparable<FragmentScan>, Spectrum {
	private final String spectrumName;
	private final String precursorName;
	private final int spectrumIndex;
	private final float scanStartTime;
	private final double precursorMz;
	private final double[] massArray;
	private final float[] intensityArray;
	private final float intensityMagnitude;
	private final float tic;
	private final byte charge;

	public FragmentScan(String spectrumName, String precursorName, int spectrumIndex, float scanStartTime, double precursorMz, double[] massArray, float[] intensityArray, byte charge) {
		this.spectrumName=spectrumName;
		this.precursorName=precursorName;
		this.spectrumIndex=spectrumIndex;
		this.scanStartTime=scanStartTime;
		this.precursorMz=precursorMz;
		this.massArray=massArray;
		this.intensityArray=intensityArray;
		this.charge=charge;
		
		float thisTic=0.0f;
		float magnitude=0.0f;
		for (float f : intensityArray) {
			thisTic+=f;
			magnitude+=f*f;
		}
		intensityMagnitude=(float)Math.sqrt(magnitude);
		tic=thisTic;
	}
	
	public FragmentScan replaceMSMSRegion(Range range, double[] newMasses, float[] newIntensities) {
		ArrayList<Peak> peaks=new ArrayList<>();
		float maxIntensity=1.0f;
		
		for (int i = 0; i < massArray.length; i++) {
			if (!range.contains(massArray[i])) {
				peaks.add(new Peak(massArray[i], intensityArray[i]));
			}
			if (intensityArray[i]>maxIntensity) {
				maxIntensity=intensityArray[i];
			}
		}
		float maxNew = Math.max(1.0f, General.max(newIntensities));
		float intensityMultiplier=maxIntensity/maxNew;
		
		for (int i = 0; i < newMasses.length; i++) {
			peaks.add(new Peak(newMasses[i], newIntensities[i]));
		}
		Collections.sort(peaks);
		TDoubleArrayList masses=new TDoubleArrayList();
		TFloatArrayList intensities=new TFloatArrayList();
		for (Peak peak : peaks) {
			masses.add(peak.mass);
			intensities.add(peak.intensity*intensityMultiplier);
		}

		return new FragmentScan(spectrumName, precursorName, spectrumIndex, scanStartTime, precursorMz, masses.toArray(), intensities.toArray(), charge);
	}
	
	private class Peak implements Comparable<Peak> {
		private final double mass;
		private final float intensity;
		public Peak(double mass, float intensity) {
			this.mass = mass;
			this.intensity = intensity;
		}
		
		@Override
		public int compareTo(Peak o) {
			if (o==null) return 1;
			return Double.compare(mass, o.mass);
		}
	}
	
	public FragmentScan mergeWith(FragmentScan alt, MassTolerance tolerance) {
		TDoubleArrayList masses=new TDoubleArrayList();
		TFloatArrayList intensities=new TFloatArrayList();

		float minRT=Float.MAX_VALUE;
		if (alt.getRetentionTimeInSec()<minRT) minRT=alt.getRetentionTimeInSec();
		
		for (FragmentScan spectrum : new FragmentScan[] {this, alt}) {
			double[] mz=spectrum.getMassArray();
			float max=General.max(spectrum.getIntensityArray())/1000f;
			float[] intens=General.divide(spectrum.getIntensityArray(), max);

			for (int i=0; i<mz.length; i++) {
				int index=getIndex(masses, mz[i], tolerance);
				if (index<0) {
					int insertionPoint=-(index+1);
					masses.insert(insertionPoint, mz[i]);
					intensities.insert(insertionPoint, intens[i]);
				} else {
					intensities.setQuick(index, intensities.getQuick(index)+intens[i]);
				}
			}
		}
		return new FragmentScan(spectrumName, precursorName, spectrumIndex, scanStartTime, precursorMz, masses.toArray(), intensities.toArray(), charge);
	}
	
	public static int getIndex(TDoubleArrayList peaks, double target, MassTolerance tolerance) {
		if (peaks.size()==0) return -1;
		
		int value=peaks.binarySearch(target);
		// exact match (not likely)
		if (value>=0) return value;
		
		int insertionPoint=-(value+1);
		
		if (insertionPoint>0) {
			// look below
			if (tolerance.compareTo(peaks.get(insertionPoint-1), target)==0) {
				return insertionPoint-1;
			}
		}
		if (insertionPoint<peaks.size()) {
			// look up
			if (tolerance.compareTo(peaks.get(insertionPoint), target)==0) {
				return insertionPoint;
			}
		}
		
		return value;
	}
	
	public FragmentScan sqrt() {
		return new FragmentScan(spectrumName, precursorName, spectrumIndex, scanStartTime, precursorMz, massArray, General.protectedSqrt(intensityArray), charge);
	}
	
	/**
	 * can return 0 (if charge state is unknown)
	 * @return
	 */
	public byte getCharge() {
		return charge;
	}
	
	@Override
	public int compareTo(FragmentScan o) {
		if (o==null) return 1;
		int c=Float.compare(scanStartTime, o.scanStartTime);
		if (c!=0) return c;
		c=Integer.compare(spectrumIndex, o.spectrumIndex);
		if (c!=0) return c;
		c=Double.compare(precursorMz, o.precursorMz);
		if (c!=0) return c;
		return spectrumName.compareTo(o.spectrumName);
	}
	
	public float getIntensityMagnitude() {
		return intensityMagnitude;
	}
	
	public float getTIC() {
		return tic;
	}

	public String getSpectrumName() {
		return spectrumName;
	}

	public String getPrecursorName() {
		return precursorName;
	}

	public int getSpectrumIndex() {
		return spectrumIndex;
	}

	public float getRetentionTimeInSec() {
		return scanStartTime;
	}

	public double getPrecursorMz() {
		return precursorMz;
	}

	public double[] getMassArray() {
		return massArray;
	}

	public float[] getIntensityArray() {
		return intensityArray;
	}
}
