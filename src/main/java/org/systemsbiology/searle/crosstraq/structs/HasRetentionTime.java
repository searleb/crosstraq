package org.systemsbiology.searle.crosstraq.structs;

public interface HasRetentionTime {

	float getRetentionTimeInSec();

}