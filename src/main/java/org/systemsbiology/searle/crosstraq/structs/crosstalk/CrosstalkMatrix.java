package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public abstract class CrosstalkMatrix {
	private static final NumberFormat NF = new DecimalFormat("0.000");
	private final String[] ionNames;
	private final double[] ionMZs;
	private final float[][] correctionFactors;
	private final float[][] correctionMatrix;
	
	public abstract float[][] getInverseCorrectionMatrix();
	public abstract CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors);
	public abstract String getName();
	
	public CrosstalkMatrix(String[] ionNames, double[] ionMZs, float[][] correctionFactors, float[][] correctionMatrix) {
		this.ionNames=ionNames;
		this.ionMZs=ionMZs;
		this.correctionFactors=correctionFactors;
		this.correctionMatrix=correctionMatrix;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	public double[] getIonMZs() {
		return ionMZs;
	}
	
	public String[] getIonNames() {
		return ionNames;
	}

	public float[][] get2n2CorrectionFactors() {
		return correctionFactors;
	}
	
	public float[][] getCorrectionMatrix() {
		return correctionMatrix;
	}
	
	public float[] correct(float[] intensities) {
		float[] abundances=new float[intensities.length];
		float[][] inverseCorrectionMatrix=getInverseCorrectionMatrix();
		int len=Math.min(intensities.length, inverseCorrectionMatrix.length);
		for (int i=0; i<len; i++) {
			for (int j=0; j<len; j++) {
				abundances[i]+=inverseCorrectionMatrix[i][j]*intensities[j];
			}
		}
		for (int i = 0; i < abundances.length; i++) {
			if (abundances[i]<0.0f) abundances[i]=0.0f;
		}
		return abundances;
	}
	
	public float[] correctUsingNNLS(float[] intensities) {
		NonNegativeLeastSquares nnls=new NonNegativeLeastSquares(getCorrectionMatrix(), intensities);
		nnls.solve();
		return nnls.x;
	}
	
	public float[] correctUsingCramerMethod(float[] intensities) {
		float[][] cm = getCorrectionMatrix();
		float[][] coefficientMatrix=transpose(cm);
		double cramerDeterminant=(new SimpleMatrix(coefficientMatrix)).determinant();
		
		float[] processed=new float[intensities.length];
		for (int i=0; i<cm.length; i++) {
			float[][] matrix=copy(coefficientMatrix); // implicit copy so we can edit it
			for (int j=0; j<cm.length; j++) {
				matrix[j][i]=intensities[j];
			}
			double det=(new SimpleMatrix(matrix)).determinant();
			processed[i]=(float)(det/cramerDeterminant);
		}

		return processed;
	}
	
	private static float[][] transpose(float[][] matrix) {
		float[][] newMatrix=new float[matrix[0].length][];
		for (int i=0; i<newMatrix.length; i++) {
			newMatrix[i]=new float[matrix.length];
			for (int j=0; j<newMatrix.length; j++) {
				newMatrix[i][j]=matrix[j][i];
			}
		}
		return newMatrix;
	}
	
	private static float[][] copy(float[][] matrix) {
		float[][] newMatrix=new float[matrix.length][];
		for (int i=0; i<newMatrix.length; i++) {
			newMatrix[i]=new float[matrix[i].length];
			for (int j=0; j<newMatrix.length; j++) {
				newMatrix[i][j]=matrix[i][j];
			}
		}
		return newMatrix;
	}

	public static float[][] getCorrectionMatrix(float[][] correctionFactors) {
		float[][] matrix=new float[correctionFactors.length][];
		for (int i=0; i<matrix.length; i++) {
			matrix[i]=new float[correctionFactors.length];
			matrix[i][i]=1.0f-General.sum(correctionFactors[i]);
			if (i>=2) matrix[i][i-2]=correctionFactors[i][0];
			if (i>=1) matrix[i][i-1]=correctionFactors[i][1];
			if (i<correctionFactors.length-1) matrix[i][i+1]=correctionFactors[i][2];
			if (i<correctionFactors.length-2) matrix[i][i+2]=correctionFactors[i][3];
		}
		return matrix;
	}
	
	public String toMatrixString() {
		String[] ionNames=getIonNames();
		double[] ionMZs=getIonMZs();
		float[][] inverseCorrectionMatrix=getInverseCorrectionMatrix();
		StringBuilder sb=new StringBuilder();
		for (int i=0; i<ionNames.length; i++) {
			sb.append('\t');
			sb.append(ionNames[i]);
		}
		sb.append('\n');
		for (int i=0; i<ionMZs.length; i++) {
			sb.append(Math.round(ionMZs[i]));
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				sb.append('\t');
				sb.append(NF.format(inverseCorrectionMatrix[i][j]));
			}
			sb.append('\n');
		}
		
		return sb.toString();
	}
}
