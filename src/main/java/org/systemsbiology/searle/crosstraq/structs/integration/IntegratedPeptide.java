package org.systemsbiology.searle.crosstraq.structs.integration;

import java.util.ArrayList;
import java.util.Arrays;

import org.systemsbiology.searle.crosstraq.structs.HasRetentionTime;
import org.systemsbiology.searle.crosstraq.structs.MZComparator;
import org.systemsbiology.searle.crosstraq.structs.Peak;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.MassConstants;
import org.systemsbiology.searle.crosstraq.utils.QuickMedian;

import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.map.hash.TByteDoubleHashMap;
import gnu.trove.procedure.TByteDoubleProcedure;

public class IntegratedPeptide implements HasRetentionTime {
	public static final byte[] integratedIsotopes=new byte[] {-1, 0, 1, 2};
	private final String peptideModSeq;
	private final ArrayList<PeptideSpectrumMatch> psms;
	private final float medianRT;
	private final Ion[] ions;
	private final ArrayList<Peak>[] peaks; // mutable!
	
	@SuppressWarnings("unchecked")
	public IntegratedPeptide(String peptideModSeq, ArrayList<PeptideSpectrumMatch> psms) {
		this.peptideModSeq = peptideModSeq;
		this.psms = psms;
		TFloatArrayList rts=new TFloatArrayList();
		TByteDoubleHashMap precursors=new TByteDoubleHashMap();
		for (PeptideSpectrumMatch psm : psms) {
			rts.add(psm.getRetentionTimeInSec());
			precursors.put(psm.getCharge(), psm.getComputedMz());
		}
		medianRT=QuickMedian.median(rts.toArray());

		ArrayList<Ion> ionList=new ArrayList<>();
		precursors.forEachEntry(new TByteDoubleProcedure() {
			@Override
			public boolean execute(byte charge, double mz) {
				for (int i = 0; i < integratedIsotopes.length; i++) {
					ionList.add(new Ion(peptideModSeq, charge, integratedIsotopes[i], mz+integratedIsotopes[i]*MassConstants.neutronMass/charge));
				}
				return true;
			}
		});
		
		ions=ionList.toArray(new Ion[ionList.size()]);
		Arrays.sort(ions, MZComparator.comparator);	
		
		peaks=new ArrayList[ions.length];
		for (int i = 0; i < ions.length; i++) {
			peaks[i]=new ArrayList<>();
		}
	}
	
	@Override
	public float getRetentionTimeInSec() {
		return medianRT;
	}
	
	public ArrayList<PeptideSpectrumMatch> getPsms() {
		return psms;
	}
	
	public String getKey() {
		return peptideModSeq;
	}
	
	public Ion[] getIons() {
		return ions;
	}
	
	public ArrayList<Peak>[] getPeaks() {
		return peaks;
	}

	public ArrayList<PeakTrace<Ion>> getCenteredTraces() {
		return getTraces(true, true);
	}
	private ArrayList<PeakTrace<Ion>> getTraces(boolean normalize, boolean center) {
		ArrayList<PeakTrace<Ion>> traces=new ArrayList<>();
		for (int i = 0; i < ions.length; i++) {
			ArrayList<Peak> peakList = peaks[i];
			PeakTrace<Ion> trace = getTrace(ions[i], peakList, normalize, center);
			traces.add(trace);
		}
		
		return traces;
	}

	private static PeakTrace<Ion> getTrace(Ion ion, ArrayList<Peak> peakList, boolean normalize, boolean center) {
		float maxIntensity=-1;
		float maxRT=0;
		for (Peak peak : peakList) {
			if (maxIntensity<peak.getIntensity()) {
				maxIntensity=peak.getIntensity();
				maxRT=peak.getRetentionTimeInSec();
			}
		}
		if (!center) maxRT=0;
		if (!normalize) maxIntensity=1.0f;
		
		TFloatArrayList x=new TFloatArrayList();
		TFloatArrayList y=new TFloatArrayList();

		for (Peak peak : peakList) {
			x.add(peak.getRetentionTimeInSec()-maxRT);
			y.add(peak.getIntensity()/maxIntensity);
		}
		PeakTrace<Ion> trace = new PeakTrace<Ion>(ion, maxRT, x.toArray(), y.toArray());
		return trace;
	}
	
	public ArrayList<IntegrationScores> integrate(IntegrationParameters params) {
		ArrayList<PeakTrace<Ion>> traces=getTraces(false, false);
		TFloatArrayList rts=new TFloatArrayList();
		for (PeptideSpectrumMatch pep : psms) {
			rts.add(pep.getRetentionTimeInSec());
		}
		return PeptideIntegrator.integratePeptide(psms.get(0).getPeptide(), rts.toArray(), traces, params);
	}
	
	public String toString() {
		ArrayList<PeakTrace<Ion>> traces=getTraces(false, false);

		StringBuilder sb=new StringBuilder("ArrayList<PeakTrace<Ion>> traces=new ArrayList<>();");
		int index=0;
		for (PeakTrace<Ion> trace : traces) {
			index++;
			Ion ion=trace.getIon();
			sb.append("Ion ion"+index+"=new Ion(\""+ion.getPeptide()+"\", (byte)"+ion.getCharge()+", (byte)"+ion.getIsotope()+", "+ion.getMZ()+");");
			sb.append("float[] rt"+index+"=new float[] {"+General.toString(trace.getRt(), "f,")+"f};");
			sb.append("float[] intensity"+index+"=new float[] {"+General.toString(trace.getIntensity(), "f,")+"f};");
			sb.append("PeakTrace<Ion> trace"+index+"=new PeakTrace<Ion>(ion"+index+", "+trace.getRetentionTimeInSec()+"f, rt"+index+", intensity"+index+");");
			sb.append("traces.add(trace"+index+");");
		}
		return sb.toString();
		
	}
}
