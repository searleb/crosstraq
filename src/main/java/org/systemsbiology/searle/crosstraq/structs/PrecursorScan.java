package org.systemsbiology.searle.crosstraq.structs;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.systemsbiology.searle.crosstraq.utils.General;

import gnu.trove.list.array.TDoubleArrayList;
import gnu.trove.list.array.TFloatArrayList;

//@Immutable
public class PrecursorScan implements Spectrum, Comparable<PrecursorScan> {
	private final String spectrumName;
	private final int spectrumIndex;
	private final float scanStartTime;
	private final double[] massArray;
	private final float[] intensityArray;
	private final float tic;

	public PrecursorScan(String spectrumName, int spectrumIndex, float scanStartTime, double[] massArray, float[] intensityArray) {
		this(spectrumName, spectrumIndex, scanStartTime, massArray, intensityArray, null);
	}

	public PrecursorScan(String spectrumName, int spectrumIndex, float scanStartTime, double[] massArray, float[] intensityArray, Float tic) {
		this.spectrumName=spectrumName;
		this.spectrumIndex=spectrumIndex;
		this.scanStartTime=scanStartTime;
		this.massArray=massArray;
		this.intensityArray=intensityArray;

		if (tic==null || !Float.isFinite(tic) || tic <= 0) {
			tic = General.sum(intensityArray);
		}
		this.tic=tic;
	}

	private static final DecimalFormat df=new DecimalFormat("0.0");
	public PrecursorScan getSubscan(double minMz, double maxMz) {
		TDoubleArrayList masses=new TDoubleArrayList();
		TFloatArrayList intensities=new TFloatArrayList();
		int insertionPoint=Arrays.binarySearch(massArray, minMz);
		if (insertionPoint<0) {
			insertionPoint=-(insertionPoint+1);
		}
		
		if (insertionPoint<0) insertionPoint=0;
		if (insertionPoint>=massArray.length) insertionPoint=massArray.length-1;
		
		for (int i=insertionPoint; i<massArray.length; i++) {
			if (massArray[i]>minMz&&massArray[i]<maxMz) {
				masses.add(massArray[i]);
				intensities.add(intensityArray[i]);
			}
			if (massArray[i]>maxMz) {
				break;
			}
		}
		return new PrecursorScan(df.format(minMz)+" to "+df.format(maxMz), spectrumIndex, scanStartTime, masses.toArray(), intensities.toArray());
	}
	
	@Override
	public float getTIC() {
		return tic;
	}
	
	@Override
	public double getPrecursorMz() {
		return -1.0;
	}

	public String getSpectrumName() {
		return spectrumName;
	}

	public int getSpectrumIndex() {
		return spectrumIndex;
	}

	public float getRetentionTimeInSec() {
		return scanStartTime;
	}

	public double[] getMassArray() {
		return massArray;
	}

	public float[] getIntensityArray() {
		return intensityArray;
	}

	@Override
	public int compareTo(PrecursorScan o) {
		if (o==null) return 1;
		int c=Float.compare(scanStartTime, o.scanStartTime);
		if (c!=0) return c;

		c=Integer.compare(spectrumIndex, o.spectrumIndex);
		if (c!=0) return c;

		c=spectrumName.compareTo(o.spectrumName);
		return c;
	}

	/**
	 * @deprecated Instead of using this method, refactor usages of its output to use a
	 *             {@code List<? extends Spectrum>} instead of a {@code List<Spectrum>}
	 *             and pass {@code precursors} to it directly.
	 */
	@Deprecated
	public static ArrayList<Spectrum> downcast(ArrayList<PrecursorScan> precursors) {
		ArrayList<Spectrum> spectra=new ArrayList<Spectrum>();
		for (Spectrum spectrum : precursors) {
			spectra.add(spectrum);
		}
		return spectra;
	}
}
