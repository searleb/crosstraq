package org.systemsbiology.searle.crosstraq.structs.crosstalk;

import org.ejml.simple.SimpleMatrix;
import org.systemsbiology.searle.crosstraq.utils.General;

public class TMT10PlexMatrix extends CrosstalkMatrix {
	private static final String[] ION_NAMES=new String[] {"TMT_126", "TMT_127N", "TMT_127C", "TMT_128N", "TMT_128C", "TMT_129N", "TMT_129C", "TMT_130N", "TMT_130C", "TMT_131N"};
	private static final double[] ION_MZS=new double[] {126.127726, 127.124761, 127.131081, 128.128116, 128.134436, 129.131471, 129.13779, 130.134825, 130.141145, 131.13818};
	private static final float[][] DEFAULT_FACTORS=new float[][] {
		General.multiply(new float[] {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 4.69f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.4f, 0.0f, 0.0f, 6.50f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.2f, 0.0f, 0.0f, 4.60f, 0.3f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.9f, 0.0f, 0.0f, 4.70f, 0.2f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.1f, 0.0f, 0.53f, 0.0f, 0.0f, 2.59f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 0.73f, 0.0f, 0.0f, 2.49f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 1.3f, 0.0f, 0.0f, 2.50f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 1.2f, 0.0f, 0.0f, 2.80f, 2.7f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.1f, 0.0f, 2.90f, 0.0f, 0.0f, 2.90f, 0.0f, 0.0f}, 0.01f),
		General.multiply(new float[] {0.0f, 0.0f, 2.36f, 0.0f, 0.0f, 1.43f, 0.0f, 0.0f}, 0.01f)};
	private static final String NAME="TMT-10plex";
	public String getName() {
		return NAME;
	}
	
	private final float[][] inverseCorrectionMatrix;
	
	public static void main(String[] args) {
		CrosstalkMatrix matrix=new TMT10PlexMatrix();
		System.out.println(matrix);
		System.out.println();
		System.out.println(General.toString(get10plexCorrectionMatrix(matrix.get2n2CorrectionFactors()), "\t"));
		System.out.println();
		System.out.println(General.toString(matrix.correct(new float[] {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f})));
	}

	public TMT10PlexMatrix() {
		this(DEFAULT_FACTORS);
	}

	private static float[][] get10plexCorrectionMatrix(float[][] correctionFactors) {
		float[][] matrix=new float[correctionFactors.length][];
		for (int i=0; i<matrix.length; i++) {
			matrix[i]=new float[correctionFactors.length];
			matrix[i][i]=1.0f-General.sum(correctionFactors[i]);
			if (i>=4) matrix[i][i-4]=correctionFactors[i][0];
			if (i>=3) matrix[i][i-3]=correctionFactors[i][1];
			if (i>=2) matrix[i][i-2]=correctionFactors[i][2];
			if (i>=1) matrix[i][i-1]=correctionFactors[i][3];
			if (i<correctionFactors.length-1) matrix[i][i+1]=correctionFactors[i][4];
			if (i<correctionFactors.length-2) matrix[i][i+2]=correctionFactors[i][5];
			if (i<correctionFactors.length-3) matrix[i][i+3]=correctionFactors[i][6];
			if (i<correctionFactors.length-4) matrix[i][i+4]=correctionFactors[i][7];
		}
		return matrix;
	}
	
	public TMT10PlexMatrix(float[][] correctionFactors) {
		super(ION_NAMES, ION_MZS, correctionFactors, get10plexCorrectionMatrix(correctionFactors));

		float[][] matrix=getCorrectionMatrix();

		SimpleMatrix s=new SimpleMatrix(matrix);
		s=s.transpose().invert();

		inverseCorrectionMatrix=new float[ION_NAMES.length][];
		for (int i=0; i<inverseCorrectionMatrix.length; i++) {
			inverseCorrectionMatrix[i]=new float[ION_NAMES.length];
			for (int j=0; j<inverseCorrectionMatrix[i].length; j++) {
				inverseCorrectionMatrix[i][j]=(float)s.get(i, j);
			}
		}
	}
	
	@Override
	public CrosstalkMatrix generateNewMatrix(float[][] new2n2CorrectionFactors) {
		return new TMT10PlexMatrix(new2n2CorrectionFactors);
	}

	public float[][] getInverseCorrectionMatrix() {
		return inverseCorrectionMatrix;
	}
}
