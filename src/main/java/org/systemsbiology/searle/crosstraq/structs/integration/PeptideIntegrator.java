package org.systemsbiology.searle.crosstraq.structs.integration;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import org.systemsbiology.searle.crosstraq.gui.Charter;
import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.Peptide;
import org.systemsbiology.searle.crosstraq.structs.PostTranslationalModification;
import org.systemsbiology.searle.crosstraq.utils.Correlation;
import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.GraphType;
import org.systemsbiology.searle.crosstraq.utils.Logger;
import org.systemsbiology.searle.crosstraq.utils.XYTrace;
import org.systemsbiology.searle.crosstraq.utils.XYTraceInterface;

import gnu.trove.list.array.TFloatArrayList;
import gnu.trove.map.hash.TByteObjectHashMap;
import gnu.trove.procedure.TByteObjectProcedure;

public class PeptideIntegrator {

	public static void main(String[] args) {
		IntegrationParameters params=new IntegrationParameters();
		Pair<Peptide, ArrayList<PeakTrace<Ion>>> pair = getExampleTrace2();
		Peptide peptide=pair.getFirst();
		ArrayList<PeakTrace<Ion>> traces=pair.getRight();
		
		long startTime=System.currentTimeMillis();
		ArrayList<IntegrationScores> allScores = integratePeptide(peptide, new float[] {8192.143f, 8201.964f}, traces, params);
		
		for (IntegrationScores scores : allScores) {
			System.out.println(scores.getIon()+") AvgCorr:"+scores.getAveragePeakCorrelation()+", AvgRT:"+scores.getApexRetentionTime()+", TotalInt:"+scores.getIntensity()+", IsoRatio:"+scores.getIsotopicRatio());
		}
		System.out.println("Delta: "+(System.currentTimeMillis()-startTime));
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<IntegrationScores> integratePeptide(Peptide peptide, float[] psmRTsInSec, ArrayList<PeakTrace<Ion>> traces, IntegrationParameters params) {
		// normalized to max of 1
		final float[] expectedIsotopicDistribution=peptide.getIsotopeDistribution();
		
		TByteObjectHashMap<PeakTrace<Ion>[]> tracesByCharge = getTracesByCharge(traces);

		ArrayList<PeakTrace<Ion>> distributionCorrectedTraces = new ArrayList<>();
		tracesByCharge.forEachEntry(new TByteObjectProcedure<PeakTrace<Ion>[]>() {
			@Override
			public boolean execute(byte charge, PeakTrace<Ion>[] traces) {
				// remove the -1 trace for quant
				PeakTrace<Ion>[] tracesCopy=new PeakTrace[traces.length-1];
				System.arraycopy(traces, 1, tracesCopy, 0, tracesCopy.length);
				
				int numPoints=tracesCopy[0].size();
				
				TFloatArrayList[] pureTraces=new TFloatArrayList[tracesCopy.length];
				for (int i = 0; i < pureTraces.length; i++) {
					pureTraces[i]=new TFloatArrayList();
				}
				
				for (int t = 0; t < numPoints; t++) { // for every RT time point (t)
					float[] intensities=new float[tracesCopy.length];
					for (int i=0; i<tracesCopy.length; i++) {
						intensities[i]=tracesCopy[i].getIntensity()[t];
					}
					
					float minScaler=Float.MAX_VALUE;
					for (int i=0; i<intensities.length; i++) {
						if (expectedIsotopicDistribution[i]>0.25f) {
							float scaler=intensities[i]/expectedIsotopicDistribution[i];
							if (minScaler>scaler) {
								minScaler=scaler;
							}
							//System.out.println(charge+","+traces[i].getRt()[t]+" = "+intensities[i]+" vs "+(expectedIsotopicDistribution[i]*minScaler)+", ("+expectedIsotopicDistribution[i]+", "+scaler+")");
						}
					}

					for (int i=0; i<tracesCopy.length; i++) {
						// can't ever have more intensity than recorded
						pureTraces[i].add(Math.min(intensities[i], expectedIsotopicDistribution[i]*minScaler));
					}
				}

				for (int i = 0; i < pureTraces.length; i++) {
					distributionCorrectedTraces.add(tracesCopy[i].updateIntensity(pureTraces[i].toArray()));
				}
				return true;
			}
		});

		Pair<ArrayList<PeakTrace<Ion>>, ArrayList<PeakTrace<Ion>>> smoothedAndNormalized = smoothAndNormalize(distributionCorrectedTraces, true, psmRTsInSec, params);
		ArrayList<PeakTrace> genericSmoothedAndNormalized=generify(smoothedAndNormalized.getSecond());
		PeakTrace<String> median=PeakTrace.getMedianTrace(genericSmoothedAndNormalized);
		RetentionTimeBoundary boundary = getBoundaries(median, psmRTsInSec, params);
		
		if (0.0f==General.max(median.getIntensity())) {
			ArrayList<PeakTrace<Ion>> originalTraces=new ArrayList<>();
			for (PeakTrace<Ion> peakTrace : traces) {
				if (peakTrace.getIon().getIsotope()>=0&&peakTrace.getIon().getIsotope()<=1) { // work from monoisotopic and +1 only
					originalTraces.add(peakTrace);
				}
			} 
			smoothedAndNormalized = smoothAndNormalize(originalTraces, false, psmRTsInSec, params);
			genericSmoothedAndNormalized=generify(smoothedAndNormalized.getSecond());
			median=PeakTrace.getMedianTrace(genericSmoothedAndNormalized); // perhaps sub with something that requires starting from a certain RT?
			boundary = getBoundaries(median, psmRTsInSec, params);
//			System.out.println(General.max(median.getIntensity())+" --> "+boundary);
//			
//			for (PeakTrace peakTrace : originalTraces) {
//				System.out.println("Trace:"+getBoundaries(peakTrace, psmRTsInSec)+", "+peakTrace.getIon()+" --> "+General.toString(peakTrace.getIntensity()));
//			}
//			Charter.launchChart(Charter.getChart("Delta RT", "Normalized Intensity (%)", true, 1, genericSmoothedAndNormalized.toArray(new PeakTrace[genericSmoothedAndNormalized.size()])), "Median");
		}
		
		final PeakTrace<String> finalMedian=PeakTrace.getMedianTrace(genericSmoothedAndNormalized, boundary);
		final RetentionTimeBoundary finalBoundary=boundary;
		
		TByteObjectHashMap<PeakTrace<Ion>[]> smoothedTracesByCharge = getTracesByCharge(smoothedAndNormalized.getFirst());

		final ArrayList<IntegrationScores> allScores=new ArrayList<>();
		smoothedTracesByCharge.forEachEntry(new TByteObjectProcedure<PeakTrace<Ion>[]>() {
			@Override
			public boolean execute(byte charge, PeakTrace<Ion>[] smoothedTraces) {
				TFloatArrayList correlationList=new TFloatArrayList();
				TFloatArrayList apexRTList=new TFloatArrayList();
				TFloatArrayList intensityList=new TFloatArrayList();
				for (PeakTrace<Ion> trace : smoothedTraces) {
					if (trace==null) continue; // for -1 trace
					
					float correlation=trace.getCorrelation(finalMedian, finalBoundary);
					correlationList.add(correlation);
					float apexRT = trace.getApexRT(finalBoundary);
					apexRTList.add(apexRT);
					float intensity=trace.integrateIntensity(finalBoundary, true);
					intensityList.add(intensity);
					//System.out.println(trace.getIon()+") Corr:"+correlation+", RT:"+apexRT+", Int:"+intensity+" --> "+trace.multiply(2).getCorrelation(finalMedian, boundary));
				}
				float averagePeakCorrelation=General.mean(correlationList.toArray());
				float meanApexRT=General.mean(apexRTList.toArray());
				float totalIntensity=General.sum(intensityList.toArray());
				
				PeakTrace<Ion>[] originalTraces=tracesByCharge.get(charge);
				float[] originalIntensities=new float[originalTraces.length];
				float[] expectedDistribution=new float[originalIntensities.length];
				for (int i = 0; i < originalTraces.length; i++) {
					byte isotope=originalTraces[i].getIon().getIsotope();
					originalIntensities[i]=originalTraces[i].integrateIntensity(finalBoundary, true);
					if (isotope>=0) {
						expectedDistribution[i]=expectedIsotopicDistribution[isotope];
					}
				}
				float isotopicRatioCorrelation=Correlation.getPearsons(originalIntensities, expectedDistribution);

				IntegrationScores scores=new IntegrationScores(smoothedTraces[1].getIon(), totalIntensity, meanApexRT, averagePeakCorrelation, isotopicRatioCorrelation, finalBoundary, finalMedian);
				allScores.add(scores);
				
				return true;
			}
		});
		
		ArrayList<XYTraceInterface> tracesForPlotting=new ArrayList<>(traces);
		double maxIntensity=1;
		for (XYTraceInterface trace : tracesForPlotting) {
			maxIntensity=Math.max(maxIntensity, General.max(trace.toArrays().getRight()));
		}
		tracesForPlotting.add(median.multiply((float)maxIntensity));
		for (int i = 0; i < psmRTsInSec.length; i++) {
			XYTrace apex=new XYTrace("PSM_"+(i+1), new double[] {psmRTsInSec[i], psmRTsInSec[i]}, new double[] {0, maxIntensity}, GraphType.dashedline, Color.BLACK, 3);
			tracesForPlotting.add(apex);
		}
		XYTrace apex=new XYTrace("Apex", new double[] {boundary.getRetentionTimeInSec(), boundary.getRetentionTimeInSec()}, new double[] {0, maxIntensity}, GraphType.dashedline, Color.RED, 3);
		tracesForPlotting.add(apex);
		XYTrace left=new XYTrace("Left", new double[] {boundary.getMinRT(), boundary.getMinRT()}, new double[] {0, maxIntensity}, GraphType.dashedline, Color.GREEN, 3);
		tracesForPlotting.add(left);
		XYTrace right=new XYTrace("Right", new double[] {boundary.getMaxRT(), boundary.getMaxRT()}, new double[] {0, maxIntensity}, GraphType.dashedline, Color.GREEN, 3);
		tracesForPlotting.add(right);
		
		
		XYTraceInterface[] traceArray=tracesForPlotting.toArray(new XYTraceInterface[tracesForPlotting.size()]);

		File context=new File("/Users/searleb/Documents/itraq/precursor_data/temp/");
		ChartPanel chart = Charter.getChart("Delta RT", "Normalized Intensity (%)", true, maxIntensity, traceArray);
		try {
			SVGGraphics2D svg2d = new SVGGraphics2D(500, 500);
			Rectangle r = new Rectangle(0, 0, 500, 500);
			chart.getChart().draw(svg2d, r);
			SVGUtils.writeToSVG(new File(context, peptide.getPeptideSequence()+".svg"), svg2d.getSVGElement());
		} catch (IOException ioe) {
			Logger.logLine("WTF --> "+peptide.getPeptideSequence());
		}

		
		return allScores;
	}

	private static Pair<ArrayList<PeakTrace<Ion>>, ArrayList<PeakTrace<Ion>>> smoothAndNormalize(
			ArrayList<PeakTrace<Ion>> distributionCorrectedTraces, boolean useSmoothed, float[] psmRTsInSec, IntegrationParameters params) {
		ArrayList<PeakTrace<Ion>> smoothedList=new ArrayList<>();
		ArrayList<PeakTrace<Ion>> smoothedAndNormalizedList=new ArrayList<>();
		for (PeakTrace<Ion> trace : distributionCorrectedTraces) {
			if (((Ion)trace.getIon()).getIsotope()>=0) {
				PeakTrace<Ion> smooth;
				if (useSmoothed) {
					smooth=trace.smooth();
				} else {
					smooth=trace;
				}
				RetentionTimeBoundary b=getBoundaries(smooth, psmRTsInSec, params);
				float apexIntensity=smooth.getIntensity(smooth.getApexRT(b));
				smoothedList.add(smooth);
				smoothedAndNormalizedList.add(smooth.multiply(1f/apexIntensity));
			}
		}
		
		Pair<ArrayList<PeakTrace<Ion>>,ArrayList<PeakTrace<Ion>>> smoothedAndNormalized=new Pair<ArrayList<PeakTrace<Ion>>, ArrayList<PeakTrace<Ion>>>(smoothedList, smoothedAndNormalizedList);
		return smoothedAndNormalized;
	}

	private static TByteObjectHashMap<PeakTrace<Ion>[]> getTracesByCharge(ArrayList<PeakTrace<Ion>> traces) {
		TByteObjectHashMap<PeakTrace<Ion>[]> tracesByCharge=new TByteObjectHashMap<>();
		for (PeakTrace<Ion> trace : traces) {
			Ion ion=trace.getIon();
			PeakTrace<Ion>[] array=tracesByCharge.get(ion.getCharge());
			if (array==null) {
				array=new PeakTrace[IntegratedPeptide.integratedIsotopes.length];
				tracesByCharge.put(ion.getCharge(), array);
			}
			array[ion.getIsotope()+1]=trace; // keep the -1 isotope
		}
		return tracesByCharge;
	}
	
	@SuppressWarnings("rawtypes")
	private static ArrayList<PeakTrace> generify(ArrayList<PeakTrace<Ion>> traces) {
		return new ArrayList<PeakTrace>(traces);
	}
	
	static int getApexIndex(float[] rts, float[] intensities, RetentionTimeBoundary psmRTRange, IntegrationParameters params) {
		float increment=(rts[rts.length-1]-rts[0])*params.getMinimumTroughRtPercentage();
		int index=Arrays.binarySearch(rts, psmRTRange.getApexRT());
		if (index<0) {
			index=-(index+1);
			if (index<0) index=0;
			if (index>=rts.length) index=rts.length-1;
		}
		
		int apexIndex=index;
		float apexIntensity=intensities[apexIndex];
		
		for (int i = index+1; i < intensities.length; i++) {
			if (intensities[i]>apexIntensity) {
				apexIndex=i;
				apexIntensity=intensities[i];
			} else if (intensities[i]<apexIntensity*params.getMinimumTroughIntensityPercentage()&&(Math.abs(rts[i]-rts[apexIndex])>increment)) {
				break;
			}
		}
		for (int i = index-1; i >= 0; i--) {
			if (intensities[i]>apexIntensity) {
				apexIndex=i;
				apexIntensity=intensities[i];
			} else if (intensities[i]<apexIntensity*params.getMinimumTroughIntensityPercentage()&&(Math.abs(rts[i]-rts[apexIndex])>increment)) {
				break;
			}
		}
		return apexIndex;
	}

	private static RetentionTimeBoundary getBoundaries(@SuppressWarnings("rawtypes") PeakTrace peak, float[] psmRTsInSec, IntegrationParameters params) {
		float[] rts=peak.getRt();
		float[] intensities=peak.getIntensity();
		RetentionTimeBoundary boundaryFromPSMs = RetentionTimeBoundary.getMedianBoundaries(psmRTsInSec);
		int apexIndex=getApexIndex(rts, intensities, boundaryFromPSMs, params);
		float apexIntensity=intensities[apexIndex];
		
		float increment=(rts[rts.length-1]-rts[0])*params.getMinimumTroughRtPercentage();
		
		int lastDownIndexRight=apexIndex;
		for (int i = apexIndex+1; i < intensities.length; i++) {
			if (intensities[i]<intensities[lastDownIndexRight]) {
				lastDownIndexRight=i;
			}
			if (intensities[lastDownIndexRight]<apexIntensity*params.getMinimumBoundaryIntensityPercentage()) {
				break;
			}

			if ((intensities[lastDownIndexRight]<apexIntensity*params.getMinimumTroughIntensityPercentage())&&(Math.abs(rts[i]-rts[lastDownIndexRight])>increment)) {
				break;
			}
		}

		
		int lastDownIndexLeft=apexIndex;
		for (int i = apexIndex-1; i >= 0; i--) {
			if (intensities[i]<intensities[lastDownIndexLeft]) {
				lastDownIndexLeft=i;
			}
			if (intensities[lastDownIndexLeft]<apexIntensity*params.getMinimumBoundaryIntensityPercentage()) {
				break;
			}

			if ((intensities[lastDownIndexLeft]<apexIntensity*params.getMinimumTroughIntensityPercentage())&&(Math.abs(rts[i]-rts[lastDownIndexLeft])>increment)) {
				break;
			}
		}
		
		// require that the boundary contains the PSMs
		RetentionTimeBoundary boundary=new RetentionTimeBoundary(Math.min(boundaryFromPSMs.getMinRT(), rts[lastDownIndexLeft]), rts[apexIndex], Math.max(boundaryFromPSMs.getMaxRT(), rts[lastDownIndexRight]));
		return boundary;
	}

	private static Pair<Peptide, ArrayList<PeakTrace<Ion>>> getExampleTrace() {
		ArrayList<PeakTrace<Ion>> traces=new ArrayList<>();
		Ion ion1=new Ion("IK[144]VC[57]QGERE", (byte)3, (byte)-1, 469.2574768219023);
		float[] rt1=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity1=new float[] {151304.9f,69827.734f,42148.402f,84196.29f,199075.47f,106456.29f,288424.97f,0.0f,3183524.0f,267433.3f,350381.88f,404459.97f,560335.3f,639236.0f,62576.816f,99648.34f,35871.715f,428878.6f,580235.7f,2097551.5f,2535659.2f,373155.38f,21102.982f,425643.7f,366363.75f,40240.785f,0.0f,0.0f,249111.44f,0.0f,153522.19f,196513.06f,179326.25f,292404.62f,0.0f,125231.98f,261944.48f,247454.9f,0.0f,0.0f};
		PeakTrace<Ion> trace1=new PeakTrace<Ion>(ion1, 0.0f, rt1, intensity1);
		traces.add(trace1);
		Ion ion2=new Ion("IK[144]VC[57]QGERE", (byte)3, (byte)0, 469.59369846053056);
		float[] rt2=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity2=new float[] {0.0f,0.0f,84113.68f,0.0f,0.0f,0.0f,0.0f,0.0f,1.235832E7f,185870.6f,81075.875f,0.0f,276065.88f,0.0f,199862.27f,0.0f,80231.92f,175147.58f,1175614.9f,5.942688E7f,6.2762344E7f,5605339.5f,665021.8f,541108.9f,0.0f,0.0f,27545.254f,0.0f,287230.06f,0.0f,361802.75f,0.0f,0.0f,0.0f,0.0f,0.0f,1517630.5f,1200418.0f,89960.92f,0.0f};
		PeakTrace<Ion> trace2=new PeakTrace<Ion>(ion2, 0.0f, rt2, intensity2);
		traces.add(trace2);
		Ion ion3=new Ion("IK[144]VC[57]QGERE", (byte)3, (byte)1, 469.92992009915884);
		float[] rt3=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity3=new float[] {203228.4f,0.0f,0.0f,0.0f,76069.53f,0.0f,0.0f,3617293.5f,2.12543984E8f,1692639.5f,0.0f,0.0f,0.0f,0.0f,461300.2f,796715.5f,0.0f,40803.305f,804431.8f,2.3008084E7f,2.3572588E7f,1865612.9f,290119.34f,27461.658f,125263.914f,0.0f,0.0f,0.0f,0.0f,113914.28f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,457734.56f,233833.38f,27136.436f,0.0f};
		PeakTrace<Ion> trace3=new PeakTrace<Ion>(ion3, 0.0f, rt3, intensity3);
		traces.add(trace3);
		Ion ion4=new Ion("IK[144]VC[57]QGERE", (byte)3, (byte)2, 470.26614173778717);
		float[] rt4=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity4=new float[] {243232.34f,54743.758f,15526.635f,356687.66f,325669.28f,543889.75f,0.0f,1443813.5f,1.0958448E8f,1747076.1f,0.0f,55625.477f,38650.586f,382118.06f,347621.5f,464363.97f,223785.12f,248618.98f,481441.97f,5287443.0f,5843681.0f,584668.44f,465353.84f,438026.03f,274962.94f,0.0f,42775.195f,370451.0f,464268.78f,167302.75f,0.0f,278847.16f,0.0f,150473.05f,0.0f,188739.4f,234777.19f,60775.1f,0.0f,0.0f};
		PeakTrace<Ion> trace4=new PeakTrace<Ion>(ion4, 0.0f, rt4, intensity4);
		traces.add(trace4);
		Ion ion5=new Ion("IK[144]VC[57]QGERE", (byte)2, (byte)-1, 703.3825772328534);
		float[] rt5=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity5=new float[] {0.0f,0.0f,0.0f,201662.28f,317239.0f,0.0f,58507.066f,288788.78f,2999563.5f,0.0f,285128.97f,257514.31f,118266.17f,0.0f,518979.1f,0.0f,0.0f,476544.38f,232918.38f,707065.9f,631079.0f,0.0f,0.0f,181157.38f,198944.7f,0.0f,0.0f,0.0f,522504.16f,223482.52f,0.0f,0.0f,330434.9f,1781672.4f,1260999.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
		PeakTrace<Ion> trace5=new PeakTrace<Ion>(ion5, 0.0f, rt5, intensity5);
		traces.add(trace5);
		Ion ion6=new Ion("IK[144]VC[57]QGERE", (byte)2, (byte)0, 703.8869096907958);
		float[] rt6=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity6=new float[] {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,7901968.0f,0.0f,191202.69f,213988.6f,117596.6f,313525.25f,0.0f,0.0f,0.0f,380528.44f,484572.25f,2.211594E7f,2.0574236E7f,1856676.8f,263826.12f,0.0f,0.0f,0.0f,444789.38f,186062.8f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,260463.38f,1074839.5f,256659.39f,229632.03f,0.0f};
		PeakTrace<Ion> trace6=new PeakTrace<Ion>(ion6, 0.0f, rt6, intensity6);
		traces.add(trace6);
		Ion ion7=new Ion("IK[144]VC[57]QGERE", (byte)2, (byte)1, 704.3912421487383);
		float[] rt7=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity7=new float[] {0.0f,0.0f,0.0f,302174.25f,296591.22f,222524.28f,250684.9f,2245073.8f,1.38098784E8f,1407591.1f,0.0f,0.0f,0.0f,0.0f,334680.12f,532285.9f,48226.9f,100410.04f,478081.03f,1.1150776E7f,7457500.0f,954223.44f,268057.8f,0.0f,222185.62f,56433.8f,61849.82f,22736.404f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,407285.75f,133258.84f,145003.25f,0.0f};
		PeakTrace<Ion> trace7=new PeakTrace<Ion>(ion7, 0.0f, rt7, intensity7);
		traces.add(trace7);
		Ion ion8=new Ion("IK[144]VC[57]QGERE", (byte)2, (byte)2, 704.8955746066807);
		float[] rt8=new float[] {2163.7769f,2166.8367f,2169.8767f,2172.907f,2175.9468f,2178.9768f,2182.0269f,2185.0176f,2188.0168f,2190.9968f,2194.0369f,2197.077f,2200.1167f,2203.1768f,2206.217f,2209.1868f,2212.2368f,2215.2769f,2218.317f,2221.327f,2224.307f,2227.347f,2230.357f,2233.427f,2236.4768f,2239.4768f,2242.4468f,2245.4468f,2248.4668f,2251.4868f,2254.5068f,2257.5166f,2260.4768f,2263.4568f,2266.4768f,2269.497f,2272.5269f,2275.5068f,2278.4468f,2281.4768f};
		float[] intensity8=new float[] {48724.71f,0.0f,222932.39f,0.0f,302229.94f,0.0f,0.0f,834299.4f,4.9589516E7f,575615.5f,52113.61f,292657.75f,0.0f,304689.12f,117901.17f,342198.7f,0.0f,112406.086f,0.0f,1923957.1f,1580734.6f,181954.44f,0.0f,0.0f,60772.97f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,111089.414f,0.0f};
		PeakTrace<Ion> trace8=new PeakTrace<Ion>(ion8, 0.0f, rt8, intensity8);
		traces.add(trace8);

		//IK[144]VC[57]QGERE  (H C O N S P)
		ArrayList<Pair<PostTranslationalModification, Integer>> modList=new ArrayList<>();
		
		modList.add(new Pair<PostTranslationalModification, Integer>(new PostTranslationalModification("iTRAQ", new int[] {12, 7, 1, 2, 0, 0}, 144.102063), 2));
		modList.add(new Pair<PostTranslationalModification, Integer>(new PostTranslationalModification("CAM", new int[] {3, 2, 1, 1, 0, 0}, 57.021464), 4));
		Peptide peptide=new Peptide("IK[144]VC[57]QGERE", modList);
		
		return new Pair<Peptide, ArrayList<PeakTrace<Ion>>>(peptide, traces);
	}
	
	private static Pair<Peptide, ArrayList<PeakTrace<Ion>>> getExampleTrace2() {
		ArrayList<PeakTrace<Ion>> traces=new ArrayList<>();
		Ion ion1=new Ion("DPNVISTINVMSLAAVGK", (byte)4, (byte)-1, 457.7492522710288);
		float[] rt1=new float[] {8128.0215f,8128.532f,8129.6587f,8130.37f,8131.3423f,8132.049f,8132.9014f,8133.702f,8134.678f,8135.397f,8136.74f,8138.0674f,8139.083f,8139.9673f,8140.5596f,8141.247f,8142.377f,8143.1846f,8144.118f,8144.573f,8145.706f,8146.377f,8147.243f,8148.2246f,8148.8857f,8149.7104f,8150.398f,8151.3677f,8152.2007f,8153.038f,8153.723f,8154.6846f,8155.8096f,8156.363f,8157.0713f,8158.03f,8158.8677f,8159.885f,8160.682f,8161.381f,8162.198f,8163.0425f,8163.86f,8164.704f,8165.3813f,8166.2056f,8167.185f,8167.89f,8168.8467f,8169.536f,8170.361f,8171.2017f,8172.1636f,8172.8584f,8173.864f,8174.534f,8175.5083f,8176.3267f,8177.413f,8178.0166f,8178.9653f,8179.649f,8180.343f,8181.3125f,8182.174f,8182.9727f,8183.8374f,8184.913f,8185.6094f,8186.166f,8187.1426f,8187.969f,8188.96f,8189.654f,8190.4717f,8191.1562f,8192.143f,8192.954f,8194.069f,8194.478f,8195.451f,8196.465f,8196.99f,8197.8125f,8198.644f,8199.619f,8200.302f,8201.144f,8201.964f,8202.94f,8204.054f,8204.752f,8205.432f,8206.551f,8206.971f,8207.938f,8208.76f,8209.764f,8210.309f,8211.122f,8212.116f,8212.794f,8213.628f,8214.742f,8215.291f,8216.129f,8216.956f,8217.796f,8218.736f,8219.584f,8220.283f,8221.122f,8222.389f,8223.208f,8224.198f,8224.768f,8225.74f,8226.294f,8227.271f,8227.952f,8228.773f,8229.769f,8230.439f,8231.571f,8232.104f,8233.102f,8233.94f,8234.603f,8235.443f,8236.269f,8237.243f,8238.1f,8238.925f,8239.933f,8240.578f,8241.68f,8242.104f,8243.064f,8243.764f,8244.769f,8245.571f,8246.537f,8247.102f};
		float[] intensity1=new float[] {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,19338.271f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
		PeakTrace<Ion> trace1=new PeakTrace<Ion>(ion1, 0.0f, rt1, intensity1);traces.add(trace1);
		Ion ion2=new Ion("DPNVISTINVMSLAAVGK", (byte)4, (byte)0, 458.00141850000006);
		float[] rt2=new float[] {8128.0215f,8128.532f,8129.6587f,8130.37f,8131.3423f,8132.049f,8132.9014f,8133.702f,8134.678f,8135.397f,8136.74f,8138.0674f,8139.083f,8139.9673f,8140.5596f,8141.247f,8142.377f,8143.1846f,8144.118f,8144.573f,8145.706f,8146.377f,8147.243f,8148.2246f,8148.8857f,8149.7104f,8150.398f,8151.3677f,8152.2007f,8153.038f,8153.723f,8154.6846f,8155.8096f,8156.363f,8157.0713f,8158.03f,8158.8677f,8159.885f,8160.682f,8161.381f,8162.198f,8163.0425f,8163.86f,8164.704f,8165.3813f,8166.2056f,8167.185f,8167.89f,8168.8467f,8169.536f,8170.361f,8171.2017f,8172.1636f,8172.8584f,8173.864f,8174.534f,8175.5083f,8176.3267f,8177.413f,8178.0166f,8178.9653f,8179.649f,8180.343f,8181.3125f,8182.174f,8182.9727f,8183.8374f,8184.913f,8185.6094f,8186.166f,8187.1426f,8187.969f,8188.96f,8189.654f,8190.4717f,8191.1562f,8192.143f,8192.954f,8194.069f,8194.478f,8195.451f,8196.465f,8196.99f,8197.8125f,8198.644f,8199.619f,8200.302f,8201.144f,8201.964f,8202.94f,8204.054f,8204.752f,8205.432f,8206.551f,8206.971f,8207.938f,8208.76f,8209.764f,8210.309f,8211.122f,8212.116f,8212.794f,8213.628f,8214.742f,8215.291f,8216.129f,8216.956f,8217.796f,8218.736f,8219.584f,8220.283f,8221.122f,8222.389f,8223.208f,8224.198f,8224.768f,8225.74f,8226.294f,8227.271f,8227.952f,8228.773f,8229.769f,8230.439f,8231.571f,8232.104f,8233.102f,8233.94f,8234.603f,8235.443f,8236.269f,8237.243f,8238.1f,8238.925f,8239.933f,8240.578f,8241.68f,8242.104f,8243.064f,8243.764f,8244.769f,8245.571f,8246.537f,8247.102f};
		float[] intensity2=new float[] {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,22975.062f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
		PeakTrace<Ion> trace2=new PeakTrace<Ion>(ion2, 0.0f, rt2, intensity2);
		traces.add(trace2);Ion ion3=new Ion("DPNVISTINVMSLAAVGK", (byte)4, (byte)1, 458.2535847289713);
		float[] rt3=new float[] {8128.0215f,8128.532f,8129.6587f,8130.37f,8131.3423f,8132.049f,8132.9014f,8133.702f,8134.678f,8135.397f,8136.74f,8138.0674f,8139.083f,8139.9673f,8140.5596f,8141.247f,8142.377f,8143.1846f,8144.118f,8144.573f,8145.706f,8146.377f,8147.243f,8148.2246f,8148.8857f,8149.7104f,8150.398f,8151.3677f,8152.2007f,8153.038f,8153.723f,8154.6846f,8155.8096f,8156.363f,8157.0713f,8158.03f,8158.8677f,8159.885f,8160.682f,8161.381f,8162.198f,8163.0425f,8163.86f,8164.704f,8165.3813f,8166.2056f,8167.185f,8167.89f,8168.8467f,8169.536f,8170.361f,8171.2017f,8172.1636f,8172.8584f,8173.864f,8174.534f,8175.5083f,8176.3267f,8177.413f,8178.0166f,8178.9653f,8179.649f,8180.343f,8181.3125f,8182.174f,8182.9727f,8183.8374f,8184.913f,8185.6094f,8186.166f,8187.1426f,8187.969f,8188.96f,8189.654f,8190.4717f,8191.1562f,8192.143f,8192.954f,8194.069f,8194.478f,8195.451f,8196.465f,8196.99f,8197.8125f,8198.644f,8199.619f,8200.302f,8201.144f,8201.964f,8202.94f,8204.054f,8204.752f,8205.432f,8206.551f,8206.971f,8207.938f,8208.76f,8209.764f,8210.309f,8211.122f,8212.116f,8212.794f,8213.628f,8214.742f,8215.291f,8216.129f,8216.956f,8217.796f,8218.736f,8219.584f,8220.283f,8221.122f,8222.389f,8223.208f,8224.198f,8224.768f,8225.74f,8226.294f,8227.271f,8227.952f,8228.773f,8229.769f,8230.439f,8231.571f,8232.104f,8233.102f,8233.94f,8234.603f,8235.443f,8236.269f,8237.243f,8238.1f,8238.925f,8239.933f,8240.578f,8241.68f,8242.104f,8243.064f,8243.764f,8244.769f,8245.571f,8246.537f,8247.102f};
		float[] intensity3=new float[] {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,24126.848f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,13398.586f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
		PeakTrace<Ion> trace3=new PeakTrace<Ion>(ion3, 0.0f, rt3, intensity3);
		traces.add(trace3);Ion ion4=new Ion("DPNVISTINVMSLAAVGK", (byte)4, (byte)2, 458.5057509579425);
		float[] rt4=new float[] {8128.0215f,8128.532f,8129.6587f,8130.37f,8131.3423f,8132.049f,8132.9014f,8133.702f,8134.678f,8135.397f,8136.74f,8138.0674f,8139.083f,8139.9673f,8140.5596f,8141.247f,8142.377f,8143.1846f,8144.118f,8144.573f,8145.706f,8146.377f,8147.243f,8148.2246f,8148.8857f,8149.7104f,8150.398f,8151.3677f,8152.2007f,8153.038f,8153.723f,8154.6846f,8155.8096f,8156.363f,8157.0713f,8158.03f,8158.8677f,8159.885f,8160.682f,8161.381f,8162.198f,8163.0425f,8163.86f,8164.704f,8165.3813f,8166.2056f,8167.185f,8167.89f,8168.8467f,8169.536f,8170.361f,8171.2017f,8172.1636f,8172.8584f,8173.864f,8174.534f,8175.5083f,8176.3267f,8177.413f,8178.0166f,8178.9653f,8179.649f,8180.343f,8181.3125f,8182.174f,8182.9727f,8183.8374f,8184.913f,8185.6094f,8186.166f,8187.1426f,8187.969f,8188.96f,8189.654f,8190.4717f,8191.1562f,8192.143f,8192.954f,8194.069f,8194.478f,8195.451f,8196.465f,8196.99f,8197.8125f,8198.644f,8199.619f,8200.302f,8201.144f,8201.964f,8202.94f,8204.054f,8204.752f,8205.432f,8206.551f,8206.971f,8207.938f,8208.76f,8209.764f,8210.309f,8211.122f,8212.116f,8212.794f,8213.628f,8214.742f,8215.291f,8216.129f,8216.956f,8217.796f,8218.736f,8219.584f,8220.283f,8221.122f,8222.389f,8223.208f,8224.198f,8224.768f,8225.74f,8226.294f,8227.271f,8227.952f,8228.773f,8229.769f,8230.439f,8231.571f,8232.104f,8233.102f,8233.94f,8234.603f,8235.443f,8236.269f,8237.243f,8238.1f,8238.925f,8239.933f,8240.578f,8241.68f,8242.104f,8243.064f,8243.764f,8244.769f,8245.571f,8246.537f,8247.102f};
		float[] intensity4=new float[] {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,25545.586f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,21717.543f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,17353.518f,0.0f};
		PeakTrace<Ion> trace4=new PeakTrace<Ion>(ion4, 0.0f, rt4, intensity4);
		traces.add(trace4);
		
		Peptide peptide=new Peptide("DPNVISTINVMSLAAVGK", new ArrayList<>());
		
		return new Pair<Peptide, ArrayList<PeakTrace<Ion>>>(peptide, traces);
	}
}
