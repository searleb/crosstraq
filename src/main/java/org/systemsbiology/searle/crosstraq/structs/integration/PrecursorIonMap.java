package org.systemsbiology.searle.crosstraq.structs.integration;

import java.util.ArrayList;
import java.util.Collections;

import org.systemsbiology.searle.crosstraq.structs.MassTolerance;
import org.systemsbiology.searle.crosstraq.structs.Pair;
import org.systemsbiology.searle.crosstraq.structs.Peak;
import org.systemsbiology.searle.crosstraq.structs.PeptideSpectrumMatch;
import org.systemsbiology.searle.crosstraq.structs.PrecursorScan;
import org.systemsbiology.searle.crosstraq.structs.RetentionTimeComparator;

// mutable! Not thread safe!
public class PrecursorIonMap {
	private final float rtHalfWindowSize;
	private final MassTolerance tolerance;
	private final ArrayList<IntegratedPeptide> peptides=new ArrayList<>();
	int currentIndex=0;
	
	public PrecursorIonMap(MassTolerance tolerance, float rtTotalWindowSize) {
		this.tolerance = tolerance;
		this.rtHalfWindowSize=rtTotalWindowSize/2.0f;
	}

	public void addPeptide(String peptideModSeq, ArrayList<PeptideSpectrumMatch> psms) {
		peptides.add(new IntegratedPeptide(peptideModSeq, psms));
	}
	
	public void finalizeMap() {
		Collections.sort(peptides, RetentionTimeComparator.comparator);
	}
	
	public void processSpectrum(PrecursorScan scan) {
		if (currentIndex>=peptides.size()) {
			return;
		}
		
		float minRT=scan.getRetentionTimeInSec()-rtHalfWindowSize;
		float maxRT=scan.getRetentionTimeInSec()+rtHalfWindowSize;
		
		while (true) {
			if (currentIndex>=peptides.size()) {
				// finished all peptides, so quit
				return;
			}
			if (peptides.get(currentIndex).getRetentionTimeInSec()>maxRT) {
				// the leading peptide is later than the scanning window, so quit
				return;
			}
			
			if (peptides.get(currentIndex).getRetentionTimeInSec()<minRT) {
				// the leading peptide is before the scanning window, so stop considering it and look at the next one
				currentIndex++;
				continue;
			}
			
			for (int i = currentIndex; i < peptides.size(); i++) {
				IntegratedPeptide pep=peptides.get(i);
				if (pep.getRetentionTimeInSec()>maxRT) {
					// beyond the current window, so quit
					return;
				}
				Ion[] ions=pep.getIons();
				double[] mzs=new double[ions.length];
				for (int j = 0; j < mzs.length; j++) {
					mzs[j]=ions[j].getMZ();
				}
				// all peptides trapped here are in the scanning window
				Pair<double[], float[]> integrations=tolerance.getIntegratedIntensities(scan.getMassArray(), scan.getIntensityArray(), mzs);
				double[] masses=integrations.getFirst();
				float[] intensities=integrations.getSecond();
				
				for (int j = 0; j < ions.length; j++) {
					pep.getPeaks()[j].add(new Peak(scan.getRetentionTimeInSec(), masses[j], intensities[j]));
				}
			}
			
			// processed all peptides against this scan, so quit
			return;
		}
	}
	
	public ArrayList<PeakTrace<Ion>> getCenteredTraces() {
		ArrayList<PeakTrace<Ion>> traces=new ArrayList<>();
		for (IntegratedPeptide peptide : peptides) {
			ArrayList<PeakTrace<Ion>> centeredTraces = peptide.getCenteredTraces();
			for (PeakTrace<Ion> trace : centeredTraces) {
				if (trace.getIon().getIsotope()>=0) {
					traces.add(trace);
				}
			}
		}
		return traces;
	}
	
	public ArrayList<IntegratedPeptide> getPeptides() {
		return peptides;
	}
}
