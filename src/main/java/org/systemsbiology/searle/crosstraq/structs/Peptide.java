package org.systemsbiology.searle.crosstraq.structs;

import java.util.ArrayList;
import java.util.HashSet;

import org.systemsbiology.searle.crosstraq.utils.General;
import org.systemsbiology.searle.crosstraq.utils.IsotopicDistributionCalculator;

public class Peptide {
	private final String peptideSequence;
	private final ArrayList<Pair<PostTranslationalModification, Integer>> modList;
	private final String key;
	private final HashSet<String> proteinAccessions; // MUTABLE (be careful)
	
	public static void main(String[] args) {
		Peptide pep=new Peptide("TAFLGPKDLFPYEE", new ArrayList<>());
		int[] mf=pep.getMolecularFormula();
		for (int i=0; i<mf.length; i++) {
			System.out.println(mf[i]);
		}
	}

	public Peptide(String peptideSequence, ArrayList<Pair<PostTranslationalModification, Integer>> modList) {
		this.peptideSequence=peptideSequence;
		this.modList=modList;
		this.proteinAccessions=new HashSet<>();

		StringBuilder sb=new StringBuilder();
		for (int i=0; i<peptideSequence.length(); i++) {
			sb.append(peptideSequence.charAt(i));
			int index=i+1;
			for (Pair<PostTranslationalModification, Integer> pair : modList) {
				if (index==pair.getSecond()) {
					sb.append("[");
					sb.append(Math.round(pair.getFirst().getDeltaMass()));
					sb.append("]");
				}
			}
		}
		key=sb.toString();
	}
	
	public void addProtein(String accession) {
		proteinAccessions.add(accession);
	}
	
	public HashSet<String> getProteinAccessions() {
		return proteinAccessions;
	}

	public String getPeptideModSeq() {
		return key;
	}

	public String getPeptideSequence() {
		return peptideSequence;
	}
	
	public int[] getMolecularFormula() {
		int[] base=AminoAcidConstants.getPeptideBaseProportions();
		
		for (char c : peptideSequence.toCharArray()) {
			int[] aaProportion=AminoAcidConstants.getAminoAcidProportions(c);
			if (aaProportion!=null) {
				base=General.add(base, aaProportion);
			}
		}
		for (Pair<PostTranslationalModification,Integer> pair : modList) {
			base=General.add(base, pair.getFirst().getComposition());
		}
		return base;
	}
	
	public double getMonoisotopicMass() {
		return IsotopicDistributionCalculator.getMonoisotopicMass(getMolecularFormula());
	}
	
	public float[] getIsotopeDistribution() {
		return IsotopicDistributionCalculator.getIsotopeDistribution(getMolecularFormula());
	}
}
