package org.systemsbiology.searle.crosstraq.structs.integration;

import junit.framework.TestCase;

public class PeptideIntegratorTest extends TestCase {
	public void testGetApexIndex() {
		IntegrationParameters params=new IntegrationParameters();
		float[] rts=new float[]         {0, 1,    2,    3,     4,    5,     6,    7,    8,    9};
		float[] intensities=new float[] {0, 0.2f, 0.3f, 0.29f, 0.4f, 0.39f, 0.3f, 0.2f, 0.1f, 0};

		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(-0.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(0.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(1.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(2.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(3.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(4.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(5.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(6.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(7.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(8.5f), params));
		assertEquals(4, PeptideIntegrator.getApexIndex(rts, intensities, getRange(9.5f), params));
	}
	
	public RetentionTimeBoundary getRange(float rt) {
		return RetentionTimeBoundary.getMedianBoundaries(new float[] {rt});
	}
}
