package org.systemsbiology.searle.crosstraq.structs.integration;

import junit.framework.TestCase;

public class PeakTraceTest extends TestCase {
	PeakTrace<String> trace=new PeakTrace<>("ion", 50, new float[] {49.6f,49.8f,50.0f,50.2f,50.4f}, new float[] {0.1f, 1f, 2f, 1.1f, 0.2f});
	
	public void testGetApex() {
		assertEquals(50f, trace.getApexRT(new RetentionTimeBoundary(50f, 50, 50f)), 0.002f);
		assertEquals(50.003f, trace.getApexRT(new RetentionTimeBoundary(49, 50, 51)), 0.002f);
		assertEquals(50.003f, trace.getApexRT(new RetentionTimeBoundary(49.5f, 50, 50.5f)), 0.002f);
		assertEquals(50.003f, trace.getApexRT(new RetentionTimeBoundary(49.6f, 50, 50.4f)), 0.002f);
		assertEquals(50.003f, trace.getApexRT(new RetentionTimeBoundary(49.9f, 50, 50.1f)), 0.002f);
	}
	
	public void testCalculateTrapezoid() {
		assertEquals(0.004999924f, PeakTrace.calculateTrapezoid(49.5f,49.6f,0.0f,0.1f), 0.0001f);
		assertEquals(0.11f, PeakTrace.calculateTrapezoid(49.6f,49.8f,0.1f,1.0f), 0.0001f);
	}
	public void testIntegrateIntensity() {
		assertEquals(0.73f, trace.integrateIntensity(new RetentionTimeBoundary(49, 50, 51), true), 0.0001f);
		assertEquals(0.73f, trace.integrateIntensity(new RetentionTimeBoundary(49.5f, 50, 50.5f), true), 0.0001f);
		assertEquals(0.73f, trace.integrateIntensity(new RetentionTimeBoundary(49.6f, 50, 50.4f), true), 0.0001f);
		assertEquals(0.44312f, trace.integrateIntensity(new RetentionTimeBoundary(49.7f, 50, 50.3f), true), 0.0001f);
		assertEquals(0.03249f, trace.integrateIntensity(new RetentionTimeBoundary(49.9f, 50, 50.1f), true), 0.0001f);
		assertEquals(0f, trace.integrateIntensity(new RetentionTimeBoundary(50f, 50, 50f), true), 0.0001f);

		assertEquals(0.85f, trace.integrateIntensity(new RetentionTimeBoundary(49, 50, 51), false), 0.0001f);
		assertEquals(0.85f, trace.integrateIntensity(new RetentionTimeBoundary(49.5f, 50, 50.5f), false), 0.0001f);
		assertEquals(0.85f, trace.integrateIntensity(new RetentionTimeBoundary(49.6f, 50, 50.4f), false), 0.0001f);
		assertEquals(0.7693757f, trace.integrateIntensity(new RetentionTimeBoundary(49.7f, 50, 50.3f), false), 0.0001f);
		assertEquals(0.36749548f, trace.integrateIntensity(new RetentionTimeBoundary(49.9f, 50, 50.1f), false), 0.0001f);
		assertEquals(0f, trace.integrateIntensity(new RetentionTimeBoundary(50f, 50, 50f), false), 0.0001f);
	}
	
	public void testGetIntensity() {
		assertEquals(1.1f, trace.getIntensity(50.2f), 0.0001f);
		assertEquals(1f, trace.getIntensity(49.8f), 0.0001f);
		
		assertEquals(1.7046977f, trace.getIntensity(50.1f), 0.0001f);
		assertEquals(1.6453238f, trace.getIntensity(49.9f), 0.0001f);
		
		assertEquals(0f, trace.getIntensity(51.1f), 0.0001f);
		assertEquals(0f, trace.getIntensity(48.9f), 0.0001f);
		
	}
}
